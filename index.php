<!DOCTYPE html>

<?php
//Copy & pasted from the above link
function multiRequest($data, $options = array()) {
  // array of curl handles
  $curly = array();
  // data to be returned
  $result = array();
  // multi handle
  $mh = curl_multi_init();
  // loop through $data and create curl handles
  // then add them to the multi-handle
  foreach ($data as $id => $d) {
    $curly[$id] = curl_init();
    $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
    curl_setopt($curly[$id], CURLOPT_URL,            $url);
    curl_setopt($curly[$id], CURLOPT_HEADER,         0);
    curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
    // post?
    if (is_array($d)) {
      if (!empty($d['post'])) {
        curl_setopt($curly[$id], CURLOPT_POST,       1);
        curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
      }
    }
    // extra options?
    if (!empty($options)) {
      curl_setopt_array($curly[$id], $options);
    }
    curl_multi_add_handle($mh, $curly[$id]);
  }
  // execute the handles
  $running = null;
  do {
    curl_multi_exec($mh, $running);
  } while($running > 0);
  // get content and remove handles
  foreach($curly as $id => $c) {
    $result[$id] = curl_multi_getcontent($c);
    curl_multi_remove_handle($mh, $c);
  }
  // all done
  curl_multi_close($mh);
  return $result;
}
?>

<?php 
    //preload post info

    $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
    $home = 'http://shunsmith.com/engine/?json=get_page&id=11';
    $about = 'http://shunsmith.com/engine/?json=get_page&id=13';
    $contact = 'http://shunsmith.com/engine/?json=get_page&id=27';
    $projects = 'http://shunsmith.com/engine/?json=get_posts&post_type=projects&custom_fields=_simple_fields_fieldGroupID_2_fieldID_1,_simple_fields_fieldGroupID_1_fieldID_2_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_4_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_5_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_6_numInSet_0';

    $data = array($home,$about,$contact,$projects);
    $output = multiRequest($data);

    $homeresult = $output[0];
    $aboutresult = $output[1];
    $contactresult = $output[2];
    $projectsresult= $output[3];

    // $homeresult = file_get_contents($home,false,$context);
    // $aboutresult = file_get_contents($about,false,$context);
    // $contactresult = file_get_contents($contact,false,$context);
    // $projectsresult= file_get_contents($projects,false,$context);
?>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="sscom" ng-controller="mainCon" >
    <head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
        <meta name="fragment" content="!" />
        <meta http-equiv="X-UA-Compatible" content="IE=8;FF=3;OtherUA=4" />
        <title ng-bind="Page.title()">Welcome | Shun Smith.com</title>
        <meta name="description" content="Thanks for stopping by Shun Smith.com I'll try to make it as interesting as possible.">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=.6">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <meta property="og:title" content="shun smith.com" />
        <meta property="og:image" content="http://shunsmith.com/img/sscom_default.png" />
        <meta property="og:url" content="http://shunsmith.com/" />

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script type="text/javascript" src="//use.typekit.net/fnu2ixs.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    </head>
    <body>
        <!--[if lt IE 9]><h1 class="intro notmobile">Hi. I love that you stopped by, but you need to step up your browser game. There are some fantastic free options out there that really enhance your web browsing experience. Besides, the sooner we stop using old browsers, the happier the web will be. Please <a href="http://browsehappy.com/">check out Browse Happy</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</h1>
        <![endif]-->
        <div class="header-container">
            <header class="wrapper clearfix">
                <h1 class="title">Angular/PHP test site</h1>
                    <div class="menu-button">Menu</div>
                 <nav>
                    <ul class="ng-view flexnav" data-breakpoint="400">
                    </ul>
                </nav>
            </header>
        </div>

        <div class="main-container">
            <div class="main wrapper clearfix">

              </div> <!-- #main -->
        </div> <!-- #main-container -->

        <div class="footer-container">
            <footer class="wrapper">
                <h3>footer</h3>
            </footer>
        </div>
        <div class="preloader on"></div>
        <script src="js/vendor/jquery-2.1.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-2.1.1.min.js"><\/script>')</script>
        <script src="js/libs.min.js"></script>
        <script src="js/main.min.js"></script>
        <script type="text/javascript">
            var dataJSON = {
                homepage: <?= json_encode($homeresult); ?>,
                about: <?= json_encode($aboutresult); ?>,
                contact: <?= json_encode($contactresult); ?>,
                projects: <?= json_encode($projectsresult); ?>
            };
        </script>

        <script>
//          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
//          ga('create', 'UA-18784661-1', 'shunsmith.com');
//          ga('send', 'pageview');
        </script>
    </body>
</html>
