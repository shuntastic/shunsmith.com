/* Acknowledgement to Mr. Doob
	revised by shun
 */
var kaleidoscope ={
	context: null,
	canvas: null,
	size:4,
	sizeHalf:0,
	resetParams: function() {
		kaleidoscope.context = null;
		if(kaleidoscope.canvas != null) document.body.removeChild(kaleidoscope.canvas);
		kaleidoscope.size = 4;
		kaleidoscope.sizeHalf = 0;
	},
	initImageKaleidoscope: function(tW,tH,imgURL,target){
			kaleidoscope.resetParams();
			kaleidoscope.canvas = document.createElement( 'canvas' );
			kaleidoscope.canvas.width = tW;
			kaleidoscope.canvas.height = tH;
			document.body.appendChild(kaleidoscope.canvas);
			
			kaleidoscope.context = kaleidoscope.canvas.getContext('2d');
			kaleidoscope.context.globalCompositeOperation = 'lighter';
			
            kaleidoscope.sizeHalf = kaleidoscope.size / 2;
            for ( var i = 0; i < 8; i ++ ) {
                kaleidoscope.size *= 2;
                kaleidoscope.sizeHalf = kaleidoscope.size / 2;
                for ( var x = 0; x < kaleidoscope.canvas.width; x += kaleidoscope.size ) {
                    for ( var y = 0; y < kaleidoscope.canvas.height; y += kaleidoscope.size ) {
                        var hue = Math.random() * ( x + y ) / 2;
                        var opacity = Math.random() * 0.5;
                        kaleidoscope.context.fillStyle = 'hsla(' + hue + ',10%,5%,' + opacity + ')';
						if (Math.random() > 0.5) kaleidoscope.drawSquare(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleTop(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleBottom(x, y);
                    }
                }
            }
			//return canvas;
			var y = kaleidoscope.convertCanvasToImage(kaleidoscope.canvas);
			//console.log('done kaleidoscope', y.src);
			return y;
			
			
			
//				$.ajax({
//						cache: false,
//						async: false,
//						dataType: 'xml',
//						crossDomain: true,
//						complete: buildFromData,
//						error: function(e) {
//						console.log('error: ',e);
//						},
//					url: 'http://mkweb.bcgsc.ca/color_summarizer/?xml=1&url='+imgURL+'precision=low'
//				});
//           function buildFromData(response) {
//				console.log('buildFromData: ',response);
//				var xmlDoc = $.parseXML(response),
//				$xml = $(xmlDoc),
//				$rgb = $xml.find('rgb');
//				
//				for ( var i = 0; i < 8; i ++ ) {
//					kaleidoscope.size *= 2;
//					kaleidoscope.sizeHalf = kaleidoscope.size / 2;
//					for ( var x = 0; x < kaleidoscope.canvas.width; x += kaleidoscope.size ) {
//						for ( var y = 0; y < kaleidoscope.canvas.height; y += kaleidoscope.size ) {
//							var rgb = $rgb[Math.floor(Math.random() * $rgb.length)];
//							var opacity = Math.random() * 0.5;
//							kaleidoscope.context.fillStyle = 'rgba(' + rgb + ',' + opacity + ')';
//							//context.fillStyle = 'hsla(' + hue + ',75%,50%,' + opacity + ')';
//							if (Math.random() > 0.5) kaleidoscope.drawSquare(x, y);
//							if (Math.random() > 0.5) kaleidoscope.drawTriangleTop(x, y);
//							if (Math.random() > 0.5) kaleidoscope.drawTriangleBottom(x, y);
//						}
//					}
//				}
//				var y = kaleidoscope.convertCanvasToImage(kaleidoscope.canvas);
//				return y;
//		   }
			
		//	var canImg = kaleidoscope.convertCanvasToImage(canvas);
		//	console.log('KALEIDOSCOPE: '+canImg.src);
		//	return canImg;
			
		},
	init: function(tW,tH,target){
		//console.log('init kaleidoscope',tW,tH);
		kaleidoscope.resetParams();
		   kaleidoscope.canvas = document.createElement('canvas');
            kaleidoscope.canvas.width = tW;
            kaleidoscope.canvas.height = tH;
            //target.appendChild(canvas);
            document.body.appendChild(kaleidoscope.canvas);
            kaleidoscope.context = kaleidoscope.canvas.getContext('2d');
            kaleidoscope.context.globalCompositeOperation = 'lighter';
            
          //  var size = 4; // change this value
            kaleidoscope.sizeHalf = kaleidoscope.size / 2;
            for ( var i = 0; i < 8; i ++ ) {
                kaleidoscope.size *= 2;
                kaleidoscope.sizeHalf = kaleidoscope.size / 2;
                for ( var x = 0; x < kaleidoscope.canvas.width; x += kaleidoscope.size ) {
                    for ( var y = 0; y < kaleidoscope.canvas.height; y += kaleidoscope.size ) {
                        var hue = Math.random() * ( x + y ) / 2;
                        var opacity = Math.random() * 0.5;
                        kaleidoscope.context.fillStyle = 'hsla(' + hue + ',75%,50%,' + opacity + ')';
						if (Math.random() > 0.5) kaleidoscope.drawSquare(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleTop(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleBottom(x, y);
                    }
                }
            }
			var y = kaleidoscope.convertCanvasToImage(kaleidoscope.canvas);
		//	console.log('done kaleidoscope', y.src);
			return y;
	},
	initBW: function(tW,tH,target){
		//console.log('initBW kaleidoscope',tW,tH);
		kaleidoscope.resetParams();
		   kaleidoscope.canvas = document.createElement('canvas');
            kaleidoscope.canvas.width = tW;
            kaleidoscope.canvas.height = tH;
            //target.appendChild(canvas);
            document.body.appendChild(kaleidoscope.canvas);
           kaleidoscope.context = kaleidoscope.canvas.getContext('2d');
            kaleidoscope.context.globalCompositeOperation = 'lighter';
            
          //  var size = 4; // change this value
            kaleidoscope.sizeHalf = kaleidoscope.size / 2;
            for ( var i = 0; i < 8; i ++ ) {
                kaleidoscope.size *= 2;
                kaleidoscope.sizeHalf = kaleidoscope.size / 2;
                for ( var x = 0; x < kaleidoscope.canvas.width; x += kaleidoscope.size ) {
                    for ( var y = 0; y < kaleidoscope.canvas.height; y += kaleidoscope.size ) {
                        var hue = Math.random() * ( x + y ) / 2;
                        var opacity = Math.random() * 0.5;
                        kaleidoscope.context.fillStyle = 'hsla(' + hue + ',10%,5%,' + opacity + ')';
						if (Math.random() > 0.5) kaleidoscope.drawSquare(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleTop(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleBottom(x, y);
                    }
                }
            }
			//return canvas;
			var y = kaleidoscope.convertCanvasToImage(kaleidoscope.canvas);
			//console.log('done kaleidoscope', y.src);
			return y;
		},
	initSetRGB: function(rgbVal,tW,tH,target){
		//console.log('initBW kaleidoscope',tW,tH);
		kaleidoscope.resetParams();
		   kaleidoscope.canvas = document.createElement('canvas');
            kaleidoscope.canvas.width = tW;
            kaleidoscope.canvas.height = tH;
            //target.appendChild(canvas);
            document.body.appendChild(kaleidoscope.canvas);
           kaleidoscope.context = kaleidoscope.canvas.getContext('2d');
            kaleidoscope.context.globalCompositeOperation = 'lighter';
            
          //  var size = 4; // change this value
            kaleidoscope.sizeHalf = kaleidoscope.size / 2;
            for ( var i = 0; i < 8; i ++ ) {
                kaleidoscope.size *= 2;
                kaleidoscope.sizeHalf = kaleidoscope.size / 2;
                for ( var x = 0; x < kaleidoscope.canvas.width; x += kaleidoscope.size ) {
                    for ( var y = 0; y < kaleidoscope.canvas.height; y += kaleidoscope.size ) {
                        var hue = Math.random() * ( x + y ) / 2;
                        var opacity = Math.random() * .2;
                        kaleidoscope.context.fillStyle = 'rgba(' + rgbVal.red + ',' + rgbVal.green + ',' + rgbVal.blue + ',' + opacity + ')';
						//kaleidoscope.context.fillStyle = 'hsla(' + hue + ',10%,5%,' + opacity + ')';
						if (Math.random() > 0.5) kaleidoscope.drawSquare(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleTop(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleBottom(x, y);
                    }
                }
            }
			//return canvas;
			var y = kaleidoscope.convertCanvasToImage(kaleidoscope.canvas);
			//console.log('done kaleidoscope', y.src);
			return y;
		},
	initSetHue: function(hueVal,tW,tH,target){
		//console.log('initBW kaleidoscope',tW,tH);
		kaleidoscope.resetParams();
		   kaleidoscope.canvas = document.createElement('canvas');
            kaleidoscope.canvas.width = tW;
            kaleidoscope.canvas.height = tH;
            //target.appendChild(canvas);
            document.body.appendChild(kaleidoscope.canvas);
           kaleidoscope.context = kaleidoscope.canvas.getContext('2d');
            kaleidoscope.context.globalCompositeOperation = 'lighter';
            
          //  var size = 4; // change this value
            kaleidoscope.sizeHalf = kaleidoscope.size / 2;
            for ( var i = 0; i < 8; i ++ ) {
                kaleidoscope.size *= 2;
                kaleidoscope.sizeHalf = kaleidoscope.size / 2;
                for ( var x = 0; x < kaleidoscope.canvas.width; x += kaleidoscope.size ) {
                    for ( var y = 0; y < kaleidoscope.canvas.height; y += kaleidoscope.size ) {
                        var sat = Math.random() * 50+50;
                        var lit = Math.random() * 50;
                        var opacity = Math.random() * .5;
                        kaleidoscope.context.fillStyle = 'hsla(' + hueVal + ',' + sat + '%,' + lit + '%,' + opacity + ')';
//                        kaleidoscope.context.fillStyle = 'hsla(' + hue + ',10%,5%,' + opacity + ')';
						if (Math.random() > 0.5) kaleidoscope.drawSquare(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleTop(x, y);
						if (Math.random() > 0.5) kaleidoscope.drawTriangleBottom(x, y);
                    }
                }
            }
			//return canvas;
			var y = kaleidoscope.convertCanvasToImage(kaleidoscope.canvas);
			//console.log('done kaleidoscope', y.src);
			return y;
		},
		drawSquare: function (x, y) {
			kaleidoscope.context.beginPath();
			kaleidoscope.context.moveTo(x, y);
			kaleidoscope.context.lineTo(x, y + kaleidoscope.size);
			kaleidoscope.context.lineTo(x + kaleidoscope.size, y + kaleidoscope.size);
			kaleidoscope.context.lineTo(x + kaleidoscope.size, y);
			//                context.lineTo( x + sizeHalf, y + sizeHalf );
			kaleidoscope.context.fill();
		},
		 drawTriangleLeft: function( x, y ) {
			kaleidoscope.context.beginPath();
			kaleidoscope.context.moveTo( x, y );
			kaleidoscope.context.lineTo( x, y + kaleidoscope.size );
			kaleidoscope.context.lineTo( x + kaleidoscope.sizeHalf, y + kaleidoscope.sizeHalf );
			kaleidoscope.context.fill();
		},
		
		drawTriangleTop: function( x, y ) {
			kaleidoscope.context.beginPath();
			kaleidoscope.context.moveTo( x, y );
			kaleidoscope.context.lineTo( x + kaleidoscope.size, y );
			kaleidoscope.context.lineTo( x + kaleidoscope.sizeHalf, y + kaleidoscope.sizeHalf );
			kaleidoscope.context.fill();
		},
		
		drawTriangleRight: function ( x, y ) {
			kaleidoscope.context.beginPath();
			kaleidoscope.context.moveTo( x + kaleidoscope.size, y );
			kaleidoscope.context.lineTo( x + kaleidoscope.size, y + kaleidoscope.size );
			kaleidoscope.context.lineTo( x + kaleidoscope.sizeHalf, y + kaleidoscope.sizeHalf );
			kaleidoscope.context.fill();
		},
		
		drawTriangleBottom: function( x, y ) {
			kaleidoscope.context.beginPath();
			kaleidoscope.context.moveTo( x, y + kaleidoscope.size );
			kaleidoscope.context.lineTo( x + kaleidoscope.sizeHalf, y + kaleidoscope.sizeHalf );
			kaleidoscope.context.lineTo( x + kaleidoscope.size, y + kaleidoscope.size );
			kaleidoscope.context.fill();
		},
		// Converts canvas to an image
		convertCanvasToImage: function(canvas) {
			var image = new Image();
			  image.src = canvas.toDataURL("image/png");
			  return image;
		}
}