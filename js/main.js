var froogaloop;
var sscom = {
  vplayer:null,
  wtplayer:null,
  viewWidth:0,
  viewHeight:0,
  k: null,
  viewCount: 0,
  updateMainBG: function(pageRef) {
    sscom.onResize();
    if(!$('.mainBG').length>0) {
      $('body').prepend('<div class="mainBG"></div>');
    }
    $('.mainBG').append('<div class="subSect mainBG'+sscom.viewCount+'"></div>');

    var introBG;
    if(pageRef=='homepage') {
      introBG = kaleidoscope.init(sscom.viewWidth/1.75,sscom.viewHeight/1.75,introBG);
    } else {
      var hueNum = Math.random()*255;
      introBG = kaleidoscope.initSetHue(hueNum,sscom.viewWidth/1.75,(sscom.viewHeight)/1.75,introBG);
    }
    $('.mainBG'+sscom.viewCount).css('background','#333 url('+introBG.src+') top center repeat');
    kaleidoscope.resetParams();
    sscom.onResize();
    sscom.viewCount++;
  },
  onResize: function(){
    sscom.viewWidth = $(window).width();
    sscom.viewHeight = $(window).height();
		// console.log('sscom.viewWidth,sscom.viewHeight',sscom.viewWidth,sscom.viewHeight);
		$('.mainSlide, .subSect').each(function(){
			if($(this).outerHeight()<sscom.viewHeight) {
				$(this).css('min-height',sscom.viewHeight);
			}
		});
    var ctr = 0;
    $('.mainSlide').each(function(){
      var curHeight = $(this).outerHeight()+10;
      $('.mainBG'+ctr).css('min-height',curHeight);
      ctr++;
    });

  },
  headerThreshold: 120,
  headerHandler: function(){
    $(window).scroll(function() {
      var value = $(this).scrollTop();
      if ( value > sscom.headerThreshold ) {
        $('header').addClass('collapse');
      } else {
        $('header').removeClass('collapse');
      }
    });
    document.addEventListener('touchmove', sscom.handleScroll);
    document.addEventListener('touchend', sscom.handleScroll);
    document.addEventListener('touchstart', sscom.handleScroll);

  },
  handleScroll: function() {
    var value = $('body').scrollTop();
    if ( value > sscom.headerThreshold ) {
      $('header').addClass('collapse');
    } else {
      $('header').removeClass('collapse');
    }

  },
  buildNewProj: function(imgArray,pTitle,pDesc1,pDesc2,pUrl,vUrl) {
    var newP = '<h1>'+pTitle+'</h1><div class="swiper-container"><div class="swiper-wrapper">'
    var slides = '';
    if(vUrl.length>0) {
      $(vUrl).each(function(index,value) {
        slides +='<div class="swiper-slide"><div class="slidescene"><iframe src="//player.vimeo.com/video/'+value+'?api=1&amp;player_id=wtplayer&amp;portrait=0&amp;title=0&amp;byline=0&amp;badge=0" id="wtplayer" width="800" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></div>';
      });
    }
    $(imgArray).each(function(index,value) {
      slides +='<div class="swiper-slide" style="background:#EFEFEF url('+value.images.full.url+') center center no-repeat;background-size:auto 98%;"></div>';
    });
    var urlBn = (pUrl !=='') ? '<a href="'+pUrl+'" target="_blank">VISIT SITE</a>&nbsp;&nbsp;|&nbsp;&nbsp;' : '';

    newP = newP + slides + '</div></div><div class="slideNav"><a class="prev-slide" href="#">PREVIOUS</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="next-slide" href="#">NEXT</a></div><div class="description"><h2>overview:</h2><p>'+pDesc1+'</p><h2>my role:</h2><p>'+pDesc2+'</p>'+urlBn+'<a href="/projects/">SEE OTHER PROJECTS</a></div>';
    return newP;
  },
  loadSlideOverlay: function(projObj,targElement){
      // console.log('overlay height: '+$('#portfolioslide').height());

      //sscom.showOverlay('#portfolioslide');
      if ($('body').hasClass('mobile')) {
        $('.slidescene, .swiper-container, .swiper-slide').css('width', (sscom.overlayWidth()*.9)+'px');
        $('.slidescene, .swiper-container, .swiper-slide').css('height', (sscom.overlayWidth()*.588)+'px');
        $.colorbox({
          width:sscom.overlayWidth(),
          height:sscom.overlayHeight(),
          inline:true,
          closeButton:true,
          close:'close',
          onClosed: function() {
            sscom.swipeControl.reInit();
            sscom.swipeControl.destroy();
            $location.path('/work');
            $scope.$apply();
          },
          href:'#portfolioslide'
        });
        sscom.swipeControl = $('#colorbox .swiper-container').swiper({
          mode:'horizontal',
          loop: false,
        });

      } else {
        $('.slidescene').css('height','400px');
        $(document).avgrund({
          width:sscom.overlayWidth(),
          height:sscom.overlayHeight(),
           // enableStackAnimation: true,
           holderClass: 'custom',
           showClose: true,
           showCloseText: 'close',
           openOnEvent: false,
           onBlurContainer: '.main-container',
           onUnload: function () {
             // sscom.swipeControl.reInit();
             sscom.swipeControl.destroy();
             $location.path('/work');
             $scope.$apply();
           },
           template: $('#portfolioslide')
         });
        sscom.swipeControl = $('.avgrund-popin .swiper-container').swiper({
          mode:'horizontal',
          loop: false,
        });
      }

      $('a.next-slide').unbind('click').click(function(e) {
        e.preventDefault();
        sscom.nextSlide();
      });
      $('a.prev-slide').unbind('click').click(function(e) {
        e.preventDefault();
        sscom.prevSlide();
      });


    },
    overlayWidth: function() {
      var val = ($('body').hasClass('mobile')) ? $(document).width() : 800;
      return val;
    },
    overlayHeight: function(tTemplate) {
      var val = ($('body').hasClass('mobile')) ? $(document).height() : (tTemplate) ? $(tTemplate).height() : 600;
      return val;
    },
    hideVideo: function(){
      if(sscom.vplayer){
        sscom.vplayer.api('unload');
      }
    },
    initProjects: function() {
      $('.ppiece').each(function(index, value){
        var controlsRef = $(this).data('work');
        var activeCheck = (sscom.folio[controlsRef].url != '') ? '&nbsp;&nbsp;<a class="visit" href="'+sscom.folio[controlsRef].url+'" target="_blank">VISIT</a>' : (sscom.folio[controlsRef].walkthrough.length >0) ? '&nbsp;&nbsp;<a class="view" href="/work/walkthrough/'+controlsRef+'">offline demo</a>' : '';
        var portfolioTemplate = '<div class="pcontrols"><h2>'+sscom.folio[controlsRef].clientName+'</h2><a class="expand" href="/work/details/'+controlsRef+'">DETAILS</a>'+activeCheck +'</div>';
        $(this).append(portfolioTemplate);
      });

      $('.ppiece').unbind('touchstart').bind( 'touchstart', function( event ) {
        $('.ppiece').each(function(index, value){
          $(this).removeClass('active');
        });
        $(this).addClass('active');
      });
    },
    currentDetails:'',
    swipeControl:null,
    prevSlide: function() {
      sscom.swipeControl.swipePrev();
    },
    nextSlide: function() {
      sscom.swipeControl.swipeNext();
    },
    closeDetails: function() {
      sscom.currentDetails = '';
    }
  };

//ANGULAR
;
'use strict';
var app = angular.module('sscom', ['ngRoute','ngResource']);
var homeNavView = '<li><a class="active" title="welcome to shunsmith.com" href="/#/">home</a></li><li><a title="about shun" href="/#/about">about</a></li><li><a title="my projects" href="/#/projects">projects</a></li><li><a title="get in touch" href="/#/contact">contact</a></li>';
var aboutNavView = '<li><a title="welcome to shunsmith.com" href="/#/">home</a></li><li><a class="active" title="about shun" href="/#/about">about</a></li><li><a title="my projects" href="/#/projects">projects</a></li><li><a title="get in touch" href="/#/contact">contact</a></li>';
var projectNavView = '<li><a title="welcome to shunsmith.com" href="/#/">home</a></li><li><a title="about shun" href="/#/about">about</a></li><li><a class="active" title="my projects" href="/#/projects">projects</a></li><li><a title="get in touch" href="/#/contact">contact</a></li>';
var contactNavView = '<li><a title="welcome to shunsmith.com" href="/#/">home</a></li><li><a title="about shun" href="/#/about">about</a></li><li><a title="my projects" href="/#/projects">projects</a></li><li><a class="active" title="get in touch" href="/#/contact">contact</a></li>';

// FACTORY
app.factory('Page', function() {
 var title = 'welcome';
 return {
   title: function() { 
      // document.title = title;
      return 'shun smith | '+title;
    },
    setTitle: function(newTitle) { title = newTitle }
  };
})
.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $routeProvider.when('/',
  {
    template: homeNavView,
    controller: 'homeCon'
  })
  .when ('/projects/',{
    template: projectNavView,
    controller:'projectCon'
  })
  .when ('/project/:clientRef',{
    template: projectNavView,
    controller:'showProject'
  })
  .when ('/about/',{
    template: aboutNavView,
    controller:'aboutCon'
  })
  .when ('/contact/',{
    template: contactNavView,
    controller:'contactCon'
  })
  .otherwise ({
    template: homeNavView,
    controller: 'homeCon'
  });
  $locationProvider.html5Mode(true);
  // $locationProvider.hashPrefix('¢');
}])
.controller('mainCon',['$scope', '$routeParams', '$location', 'Page', '$window', function ($scope, $routeParams, $location, Page, $window) {
  $scope.mobileCon = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ? ((/IEMobile/i.test(navigator.userAgent)) ? 'mobile ieMobile' : 'mobile') : 'desktop';
  $scope.Page = Page;
  $scope.siteLoaded =false;
  $scope.$on('$viewContentLoaded', function(event) {
   $window.ga('send', 'pageview', { page: $location.path() });
 });


  $scope.mainSliderShow = function(ref,$http,$scope,projName){
    function displayPage(ref,data) {
      var feedReturn = $(data);
      var feedData = feedReturn[0];

      var newCont ='';
      switch(ref) {
        case 'project':
        var desc1 = feedData.post.custom_fields._simple_fields_fieldGroupID_1_fieldID_2_numInSet_0[0];
        var desc2 = feedData.post.custom_fields._simple_fields_fieldGroupID_1_fieldID_5_numInSet_0[0];
        var url = feedData.post.custom_fields._simple_fields_fieldGroupID_1_fieldID_4_numInSet_0[0];
        var vUrl = ((feedData.post.custom_fields._simple_fields_fieldGroupID_1_fieldID_6_numInSet_0!==undefined)&&(feedData.post.custom_fields._simple_fields_fieldGroupID_1_fieldID_6_numInSet_0[0] !=''))? (feedData.post.custom_fields._simple_fields_fieldGroupID_1_fieldID_6_numInSet_0[0]).split(',') : [];
        var newProj = sscom.buildNewProj(feedData.post.attachments,feedData.post.title_plain,desc1,desc2,url,vUrl);
        newCont = newProj;

        break;
        case 'projects':
          //Call custom post type list
          var projectList = '';
          $.each(feedData.posts, function(index,val){
            var desc1 = this.custom_fields._simple_fields_fieldGroupID_1_fieldID_2_numInSet_0[0];
            var desc2 = this.custom_fields._simple_fields_fieldGroupID_1_fieldID_5_numInSet_0[0];
            var url = this.custom_fields._simple_fields_fieldGroupID_1_fieldID_4_numInSet_0[0];
            var urlBn = (url !=='') ? '<a href="'+url+'" target="_blank">VISIT SITE</a>&nbsp;&nbsp;|&nbsp;&nbsp;' : '';

            var indProject = '<div class="indproj '+this.slug+'"><h3>'+this.title+'</h3><img onload="sscom.onResize()" src="'+this.attachments[0].images.thumbnail.url+'" /><div class="description">'+desc1+'<div class="jobrole"><h4>my role:</h4>'+desc2+'</div>'+urlBn+'<a href="/project/'+this.slug+'">SEE MORE</a></div></div>';
            projectList +=indProject;

          });
          newCont = '<h1>my projects:</h1><p>This is a sample of some of the projects I\'ve worked on over the last few years, most with Coloring Book Studio, where I was a partner and Technology Director.</p><div class="isogrid">'+projectList+'</div>';

          break;
          default:
          newCont = '<h1>'+feedData.page.title+'</h1>'+feedData.page.content;
          break;
        }
        el = $(elPath);

        el.html(newCont);
        sscom.updateMainBG(ref);
        if(ref=='projects') {
          var $container = angular.element('.isogrid').imagesLoaded( function() {
            $container.isotope({
              itemSelector: '.indproj',
              layoutMode: 'fitRows',
              masonry: {
                columnWidth: 250
              }
            }).isotope( 'on', 'layoutComplete', function(){sscom.onResize();});
            sscom.onResize();
          });
        }
        if(ref=='project') {
          sscom.swipeControl = $(elPath+' .swiper-container').swiper({
            mode:'horizontal',
            loop: false,
          });

          $(elPath+' a.next-slide').unbind('click').click(function(e) {
            e.preventDefault();
            sscom.nextSlide();
          });
          $(elPath+' a.prev-slide').unbind('click').click(function(e) {
            e.preventDefault();
            sscom.prevSlide();
          });
        }
        
        $scope.mainSliderShow(ref,$http,$scope,projName);

      }





      $('.flexnav').flexNav({
        'animationSpeed':0,
        'transitionOpacity': false,
        'hoverIntent': false,
        'hoverIntentTimeout': 0
      })
      $('.flexnav').flexNav({'animationSpeed':'fast'});




      projName = typeof projName !== 'undefined' ? projName : '';
      dataJSON[ref] = typeof dataJSON[ref] !== 'undefined' ? dataJSON[ref] : ''
      var elPath = ('.main .mainSlide.'+ref)+((projName !=='')?'.'+projName:'');
    // console.log('el path',elPath)

    var el =  $(elPath);
    if(el.length>0) {
      $('.preloader').removeClass('on');
      var offset = el.offset();
      $('html,body').animate({ scrollTop: (offset.top),easing:'easeOut' }, 900);
    } else {

      $('.preloader').addClass('on');
      var newel = '<div class="mainSlide '+ref+' '+projName+'"></div>';

      $('.main-container div.main').append(newel);
      
      if(dataJSON[ref]!='') {
        var feedReturn = JSON.parse(dataJSON[ref]);
        displayPage(ref,feedReturn);
      } else {
        var qstring='';
        switch(ref) {
          case 'project':
          // newel = 
          qstring = '?json=get_post&post_type=projects&post_slug='+projName+'&custom_fields=_simple_fields_fieldGroupID_2_fieldID_1,_simple_fields_fieldGroupID_1_fieldID_2_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_4_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_5_numInSet_0,_simple_fields_fieldGroupID_3_fieldID_1,_simple_fields_fieldGroupID_1_fieldID_6_numInSet_0';
          break;
          case 'projects':
          qstring = '?json=get_posts&post_type='+ref+'&custom_fields=_simple_fields_fieldGroupID_2_fieldID_1,_simple_fields_fieldGroupID_1_fieldID_2_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_4_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_5_numInSet_0,_simple_fields_fieldGroupID_1_fieldID_6_numInSet_0';
          break;
          default:
          qstring = '?json=get_page&page_slug='+ref;
          break;

        }
        // $http({method: 'GET', url: 'http://shunsmith.com/engine/'+ref+'/'})
        var getPath = 'http://shunsmith.com/engine/'+qstring;
        // console.log('GET string',getPath);
        $http({method: 'GET', url: getPath})
        .success(function(data, status, headers, config) {
          displayPage(ref,data);
        }).
        error(function(data, status, headers, config) {
          console.log('USER INFO ERROR '+status)
        });

      }

    }

  }

  $scope.homeCon = function(Page,$scope,$http) {
    $scope.mainSliderShow('homepage',$http,$scope);
    Page.setTitle('welcome');
  };
  $scope.aboutCon = function(Page,$scope,$http) {
   $scope.mainSliderShow('about',$http,$scope);
   Page.setTitle('about me');
 };
 $scope.projectCon = function(Page,$scope,$http) {
  $scope.mainSliderShow('projects',$http,$scope);
  Page.setTitle('my projects');
};
$scope.contactCon = function(Page,$scope,$http) {
  Page.setTitle('get in touch');
  $scope.mainSliderShow('contact',$http,$scope);
};
  // $scope.projectCon = function(Page) {
  //   sscom.mainSliderShow(2);
  //   Page.setTitle('Work');
  // };
  $scope.showProject = function(Page,$scope,$http) {
    $scope.mainSliderShow('project',$http,$scope,$routeParams.clientRef);
    Page.setTitle('project: '+($routeParams.clientRef));


  };
}]);

//	INIT
(function($) {
  $(document).ready(function() {
    sscom.onResize();
    sscom.headerHandler();
    $(window).resize(function() {sscom.onResize();});
  });
}(jQuery));