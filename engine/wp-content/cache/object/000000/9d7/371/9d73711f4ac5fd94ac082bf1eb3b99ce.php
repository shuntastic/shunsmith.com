)�5T<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:13;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-09-01 15:19:29";s:13:"post_date_gmt";s:19:"2014-09-01 15:19:29";s:12:"post_content";s:2906:"<h2 class="p1">2007 – present: Coloring Book Studio, Austin, Texas</h2>
<h3 class="p2">Co-founder | Technology Director</h3>
<p class="p3">Lead developer and manager of development services for studio projects • Manage asset production services for studio projects • Research and maintain hardware and software technologies for the studio</p>
<p class="p3"><strong>Client List</strong>: Air Force, Texas Department of State Health Services, HomeAway, Dell, Bunkhouse Management, Mattel Barbie, Paramount Pictures, GSD&amp;M, Sherry Matthews Advocacy Marketing, Clif Bar Family Foundation, Generations Federal Credit Union, SkyVenture</p>

<h2 class="p1">2005 – 2007: nFusion Group, Austin, Texas</h2>
<h3 class="p2">Senior Interactive Production Designer</h3>
<p class="p3">Established Interactive production department for company • Produced and designed online banners and various Flash-based assets</p>
<p class="p3"><strong>Client list:</strong> Toshiba, NEC, Nokia, Wilshire Homes, Vericenter</p>

<h2 class="p1">2005 – 2007: CSI Designs Austin, Texas</h2>
<h3 class="p2">Co-founder | Designer</h3>
<p class="p3">Designed and developed websites, logos and print materials for local businesses.</p>

<h2 class="p1">1996 – 2005: GSD&amp;M, Austin, Texas</h2>
<h3 class="p2">Interactive Studio Artist</h3>
<p class="p3">Produced and designed banner ads for various clients • Produced websites for new and prospective clients • Created Macromedia Flash-based demos • Worked directly with some vendors to gather specs</p>

<h3 class="p2">Print Studio Artist</h3>
<p class="p3">Laid out and revised print ads • Retouched and created images and artwork for comps and press-ready purposes</p>

<h3 class="p2">Image Technician | Intern</h3>
<p class="p3">Scanned and retouched images for use in comps. Dark Room mechanical production</p>
<p class="p3"><strong>Client List:</strong> AARP, Air Force, Beau Rivage Casino and Resorts, Chili’s, Fannie Mae Foundation, Kinko’s, Krispy Kreme, Land Rover, Lennox, Macaroni Grill, PGA, SAM’S CLUB, SBC, SeaWorld, Southwest Airlines, Southwestern Bell, United Healthcare Foundation</p>

<h2>Development Languages:</h2>
<p class="p3">HTML, HTML5, CSS, CSS3, Javascript, AS 2, AS 3, PHP</p>

<h2 class="p3">Development Libraries/Frameworks:</h2>
<p class="p3">jQuery, Wordpress, AngularJS, LESS, misc. data APIs</p>

<h2 class="p3">Application/Platform Knowledge:</h2>
<p class="p3">Sublime Text, MAMP, Flash, After Effects, Premiere Pro, Fireworks, Photoshop, Illustrator, InDesign, Soundbooth, Git, BitBucket, and various sound and music editing software and hardware</p>

<h2 class="p1">Education</h2>
<h3 class="p1">1992 – 1997: University of Texas at Austin</h3>
<p class="p3">Bachelor of Arts candidate in College of Liberal Arts,</p>
<p class="p3">American Studies major, Computer Science minor</p>";s:10:"post_title";s:5:"about";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:5:"about";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-09-02 19:53:48";s:17:"post_modified_gmt";s:19:"2014-09-02 19:53:48";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:39:"http://shunsmith.com/engine/?page_id=13";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}