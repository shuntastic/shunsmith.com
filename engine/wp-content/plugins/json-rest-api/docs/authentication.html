<h1>Authentication</h1>

<p>There are two options for authenticating with the API. The basic choice boils
down to:</p>

<ul>
<li>Are you a plugin/theme running on the site? Use <strong>cookie authentication</strong></li>
<li>Are you a desktop/web/mobile client accessing the site externally? Use
<strong>OAuth authentication</strong></li>
</ul>


<h2>Cookie Authentication</h2>

<p>Cookie authentication is the basic authentication method included with
WordPress. When you log in to your dashboard, this sets up the cookies correctly
for you, so plugin and theme developers need only to have a logged-in user.</p>

<p>However, WP API includes a technique called <a href="http://codex.wordpress.org/WordPress_Nonces">nonces</a> to avoid <a href="http://en.wikipedia.org/wiki/Cross-site_request_forgery">CSRF</a> issues.
This prevents other sites from forcing you to perform actions without explicitly
intending to do so. This requires slightly special handling for the API.</p>

<p>For developers using the built-in Javascript API, this is handled automatically
for you. This is the recommended way to use the API for plugins and themes.
Custom data models can extend <code>wp.api.models.Base</code> to ensure this is sent
correctly for any custom requests.</p>

<p>For developers making manual Ajax requests, the nonce will need to be passed
with each request. The API uses nonces with the action set to <code>wp_api</code>. These
can then be passed to the API via the <code>_wp_json_nonce</code> data parameter (either
POST data or in the query for GET requests), or via the <code>X-WP-Nonce</code> header.</p>

<p>As an example, this is how the built-in Javascript client creates the nonce:</p>

<p><code>php
wp_localize_script( 'wp-api', 'wpApiOptions', array( 'base' =&gt; json_url(), 'nonce' =&gt; wp_create_nonce( 'wp_json' ) ) );
</code></p>

<p>This is then used in the base model:</p>

<p>```javascript
options.beforeSend = function(xhr) {
    xhr.setRequestHeader(&lsquo;X-WP-Nonce&rsquo;, wpApiOptions.nonce);</p>

<pre><code>if (beforeSend) {
    return beforeSend.apply(this, arguments);
}
</code></pre>

<p>};
```</p>

<h2>OAuth Authentication</h2>

<p>OAuth authentication is the main authentication handler used for external
clients. With OAuth authentication, users still only ever log in via the normal
WP login form, and then authorize clients to act on their behalf. Clients are
then issued with OAuth tokens that enable them to access the API. This access
can be revoked by users at any point.</p>

<p>OAuth authentication uses the <a href="http://tools.ietf.org/html/rfc5849">OAuth 1.0a specification</a> (published as
RFC5849) and requires installing the <a href="https://github.com/WP-API/OAuth1">OAuth plugin</a> on the site.
(This plugin will be included with the API when merged into core.)</p>

<p>Once you have WP API and the OAuth server plugins activated on your server,
you&rsquo;ll need to create a &ldquo;consumer&rdquo;. This is an identifier for the application,
and includes a &ldquo;key&rdquo; and &ldquo;secret&rdquo;, both needed to link to your site.</p>

<p>To create the consumer, run the following on your server:
```bash
$ wp oauth1 add</p>

<p>ID: 4
Key: sDc51JgH2mFu
Secret: LnUdIsyhPFnURkatekRIAUfYV7nmP4iF3AVxkS5PRHPXxgOW
```</p>

<p>This key and secret is your consumer key and secret, and needs to be used
throughout the authorization process. Currently no UI exists to manage this,
however this is planned for a future release.</p>

<p>For examples on how to use this, both the <a href="https://github.com/WP-API/client-cli">CLI client</a> and the
<a href="https://github.com/WP-API/api-console">API console</a> make use of the OAuth functionality, and are a great
starting point.</p>

<h2>Basic Authentication</h2>

<p>Basic Authentication is an optional authentication handler for external clients.
Due to the complexity of OAuth authentication, Basic authentication can be
useful during development. However, Basic authentication requires passing your
username and password on every request, as well as giving your credentials to
clients, so it is heavily discouraged for production use.</p>

<p>Basic authentication uses <a href="https://tools.ietf.org/html/rfc2617">HTTP Basic Authentication</a> (published as
RFC2617) and requires installing the <a href="https://github.com/WP-API/Basic-Auth">Basic Auth plugin</a>.</p>

<p>To use Basic authentication, simply pass the username and password with each
request through the <code>Authorization</code> header. This value should be encoded as per
the HTTP Basic specification.</p>
