<h1>Introduction</h1>

<p>The API is designed around two types of responses: entities, and collections.
Entities are JSON objects representing internal objects, both abstract and
WordPress objects. Collections are JSON arrays of Entities.</p>

<p>This document is for clients and providers wanting to ensure full compliance
with the specification.</p>

<h1>Definitions</h1>

<p>The key words &ldquo;MUST&rdquo;, &ldquo;MUST NOT&rdquo;, &ldquo;REQUIRED&rdquo;, &ldquo;SHALL&rdquo;, &ldquo;SHALL NOT&rdquo;, &ldquo;SHOULD&rdquo;,
&ldquo;SHOULD NOT&rdquo;, &ldquo;RECOMMENDED&rdquo;, &ldquo;MAY&rdquo;, and &ldquo;OPTIONAL&rdquo; in this document are to be
interpreted as described in <a href="http://tools.ietf.org/html/rfc2119">RFC2119</a>.</p>

<ul>
<li>Provider: A site making the API available for use</li>
<li>Consumer: An application accessing and interacting with the API</li>
<li>slug: A URL-friendly human-readable identifier, usually derived from the title
      of the entity.</li>
</ul>


<h3>ABNF</h3>

<p>Augmented Backus-Naur Form (ABNF) is to be interpreted as described in
<a href="http://tools.ietf.org/html/rfc5234">RFC5234</a>. In addition, the following basic rules are used to describe basic
parsing constructs above the standard JSON parsing rules.</p>

<pre><code>token = 1*&lt;any OCTET except CTLs&gt; ; DQUOTE must be escaped with "\"
</code></pre>

<p>Note that as per ABNF, literal strings are case insensitive. That is:</p>

<pre><code>example-field = "id"
example-field = "ID"
</code></pre>

<p>Providers SHOULD use the capitalisation as per this specification to ensure
maximum compatibility with consumers. Consumers SHOULD ignore the case of
literal strings when parsing data.</p>

<h1>Entities</h1>

<h2>Index</h2>

<p>The Index entity is a JSON object with site properties. The following properties
are defined for the Index entity object.</p>

<h3><code>name</code></h3>

<p>The <code>name</code> field is a string with the site&rsquo;s name.</p>

<h3><code>description</code></h3>

<p>The <code>description</code> field is a string with the site&rsquo;s description.</p>

<h3><code>URL</code></h3>

<p>The <code>URL</code> field is a string with the URL to the site itself.</p>

<h3><code>routes</code></h3>

<p>The <code>routes</code> field is an object with keys as a route and the values as a route
descriptor.</p>

<p>The route is a string giving the URL template for the route, relative to the API
root. The template contains URL parts separated by forward slashes, with each
URL part either a static string, or a route variable encased in angle brackets.</p>

<pre><code>route            = ( "/"
                 / *( "/" ( token / route-variable ) ) )
route-variable   = "&lt;" token "&gt;"
</code></pre>

<p>These routes can be converted into URLs by replacing all route variables with
their relevant values, then concatenating the relative URL to the API base.</p>

<p>The route descriptor is an object with the following defined properties.</p>

<ul>
<li><code>supports</code>: A JSON array of supported HTTP methods (verbs). Possible values
are &ldquo;HEAD&rdquo;, &ldquo;GET&rdquo;, &ldquo;POST&rdquo;, &ldquo;PUT&rdquo;, &ldquo;PATCH&rdquo;, &ldquo;DELETE&rdquo;</li>
<li><code>accepts_json</code>: A boolean indicating whether data can be passed directly via a
POST request body. Default for missing properties is false.</li>
<li><code>meta</code>: An Entity Meta entity. Typical <code>links</code> values consist of a <code>self</code> link
pointing to the route&rsquo;s full URL.</li>
</ul>


<h3><code>meta</code></h3>

<p>The <code>meta</code> field is a Entity Meta entity with metadata relating to the entity
representation.</p>

<p>Typical <code>links</code> values for the meta object consist of a <code>help</code> key with the
value indicating a human-readable documentation page about the API.</p>

<h3>Example</h3>

<pre><code>{
    "name": "My WordPress Site",
    "description": "Just another WordPress site",
    "URL": "http:\/\/example.com",
    "routes": {
        "\/": {
            "supports": [
                "HEAD",
                "GET"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/"
            }
        },
        "\/posts": {
            "supports": [
                "HEAD",
                "GET",
                "POST"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/posts"
            },
            "accepts_json": true
        },
        "\/posts\/&lt;id&gt;": {
            "supports": [
                "HEAD",
                "GET",
                "POST",
                "PUT",
                "PATCH",
                "DELETE"
            ],
            "accepts_json": true
        },
        "\/posts\/&lt;id&gt;\/revisions": {
            "supports": [
                "HEAD",
                "GET"
            ]
        },
        "\/posts\/&lt;id&gt;\/comments": {
            "supports": [
                "HEAD",
                "GET",
                "POST"
            ],
            "accepts_json": true
        },
        "\/posts\/&lt;id&gt;\/comments\/&lt;comment&gt;": {
            "supports": [
                "HEAD",
                "GET",
                "POST",
                "PUT",
                "PATCH",
                "DELETE"
            ],
            "accepts_json": true
        },
    },
    "meta": {
        "links": {
            "help": "https:\/\/github.com\/WP-API\/WP-API",
            "profile": "https:\/\/raw.github.com\/WP-API\/WP-API\/master\/docs\/schema.json"
        }
    }
}
</code></pre>

<h2>Post</h2>

<p>The Post entity is a JSON object of post properties. Unless otherwise defined,
properties are available in all contexts. The following properties are defined
for the Post entity object:</p>

<h3><code>title</code></h3>

<p>The <code>title</code> field is a string with the post&rsquo;s title.</p>

<h3><code>date</code>, <code>date_gmt</code></h3>

<p>The <code>date</code> and <code>date_gmt</code> fields are strings with the post&rsquo;s creation date and
time in the local time and UTC respectively. These fields follow the <a href="http://tools.ietf.org/html/rfc3339">RFC3339</a>
Section 5.6 datetime representation.</p>

<pre><code>date     = date-time
date_gmt = date-time
</code></pre>

<h3><code>modified</code>, <code>modified_gmt</code></h3>

<p>The <code>modified</code> and <code>modified_gmt</code> fields are strings with the post&rsquo;s last
modification date and time in the local time and UTC respectively. These fields
follow the <a href="http://tools.ietf.org/html/rfc3339">RFC3339</a> Section 5.6 datetime representation.</p>

<pre><code>modified     = date-time
modified_gmt = date-time
</code></pre>

<h3><code>date_tz</code>, <code>modified_tz</code></h3>

<p>The <code>date_tz</code> and <code>modified_tz</code> fields are strings with the timezone applying to
the <code>date</code> and <code>modified</code> fields respectively. The timezone is a <a href="https://en.wikipedia.org/wiki/Tz_database">Olsen zoneinfo
database</a> identifier. While the <code>date</code> and <code>modified</code> fields include timezone
offset information, the <code>date_tz</code> and <code>modified_tz</code> fields allow proper data
operations across Daylight Savings Time boundaries.</p>

<p>Note that in addition to the normal Olsen timezones, manual offsets may be
given. These manual offsets use the deprecated <code>Etc/GMT+...</code> zones and specify
an integer offset in hours from UTC.</p>

<pre><code>timezone      = Olsen-timezone / manual-offset
manual-offset = "Etc/GMT" ("-" / "+") 1*2( DIGIT )
</code></pre>

<p>Consumers SHOULD use the fields if they perform mathematical operations on the
<code>date</code> and <code>modified</code> fields (such as adding an hour to the last modification
date) rather than relying on the <code>time-offset</code> in the <code>date</code> or
<code>modified</code> fields.</p>

<h3><code>status</code></h3>

<p>The <code>status</code> field is a string with the post&rsquo;s status. This status relates to
where the post is in the editorial process. These are usually set values, but
some providers may have extra post statuses.</p>

<pre><code>post-status = "draft" / "pending" / "private" / "publish" / "trash" / token
</code></pre>

<p>Consumers who encounter an unknown or missing post status SHOULD treat it the
same as a &ldquo;draft&rdquo; status.</p>

<h3><code>type</code></h3>

<p>The <code>type</code> field is a string with the post&rsquo;s type. This field is specific to
providers, with the most basic representation being &ldquo;post&rdquo;. The type of the
post usually relates to the fields in the Post entity, with other types having
additional fields specific to the type.</p>

<pre><code>post-type = "post" / token
</code></pre>

<p>Consumers who encounter an unknown or missing post type SHOULD treat it the same
as a &ldquo;post&rdquo; type.</p>

<h3><code>name</code></h3>

<p>The <code>name</code> field is a string with the post&rsquo;s slug.</p>

<h3><code>author</code></h3>

<p>The <code>author</code> field is a User entity with the user who created the post.</p>

<h3><code>password</code></h3>

<p>The <code>password</code> field is a string with the post&rsquo;s password. A zero-length
password indicates that the post does not have a password.</p>

<p>Consumers who encounter a missing password MUST treat it the same as a
zero-length password.</p>

<h3><code>content</code></h3>

<p>The <code>content</code> field is a string with the post&rsquo;s content.</p>

<h3><code>excerpt</code></h3>

<p>The <code>excerpt</code> field is a string with the post&rsquo;s excerpt. This is usually a
shortened version of the post content, suitable for displaying in
collection views.</p>

<p>Consumers who encounter a missing excerpt MAY present a shortened version of the
<code>content</code> field instead.</p>

<h3><code>content_raw</code>, <code>excerpt_raw</code></h3>

<p>The <code>content_raw</code> and <code>excerpt_raw</code> fields are strings with the post&rsquo;s content
and excerpt respectively. Unlike the <code>content</code> and <code>excerpt</code> fields, the value
has not been passed through internal filtering, and is suitable for editing.</p>

<p>(Context Availability: <code>edit</code>)</p>

<h3><code>parent</code></h3>

<p>The <code>parent</code> field is an integer or JSON object with the post&rsquo;s parent
post ID. A literal zero indicates that the post does not have a parent
post.</p>

<pre><code>post-parent = "0" / 1*DIGIT
</code></pre>

<p>Consumers who encounter a missing parent ID MUST treat it the same as a parent
post ID of 0.</p>

<p>Parent fields will be expanded into a full Post entity in the <code>view</code> or <code>edit</code>
contexts, but only one level deep. The embedded Post entity will be rendered
using the <code>parent</code> context.</p>

<p>In the <code>parent</code> context, the field will contain an integer with the post&rsquo;s
parent post ID as above.</p>

<h3><code>link</code></h3>

<p>The <code>link</code> field is a string with the full URL to the post&rsquo;s canonical view.
This is typically the human-readable location of the entity.</p>

<h3><code>guid</code></h3>

<p>The <code>guid</code> field is a string with the post&rsquo;s globally unique identifier (GUID).</p>

<p>The GUID is typically in URL form, as this is a relatively easy way of ensuring
that the GUID is globally unique. However, consumers MUST NOT treat the GUID as
a URL, and MUST treat the GUID as a string of arbitrary characters.</p>

<h3><code>menu_order</code></h3>

<p>The <code>menu_order</code> field is an integer with the post&rsquo;s sorting position. This is
typically used to affect sorting when displaying the post in menus or lists.
Larger integers should be treated as sorting before smaller integers.</p>

<pre><code>menu-order = 1*DIGIT / "-" 1*DIGIT
</code></pre>

<p>Consumers who encounter a missing sorting position MUST treat it the same as a
sorting position of 0.</p>

<h3><code>comment_status</code></h3>

<p>The <code>comment_status</code> field is a string with the post&rsquo;s current commenting
status. This field indicates whether users can submit comments to the post.</p>

<pre><code>post-comment-status = "open" / "closed" / token
</code></pre>

<p>Providers MAY use statuses other than &ldquo;open&rdquo; or &ldquo;closed&rdquo; to indicate other
statuses. Consumers who encounter an unknown or missing comment status SHOULD
treat it as &ldquo;closed&rdquo;.</p>

<h3><code>ping_status</code></h3>

<p>The <code>ping_status</code> field is a string with the post&rsquo;s current pingback/trackback
status. This field indicates whether users can submit pingbacks or trackbacks
to the post.</p>

<pre><code>ping-status = "open" / "closed" / token
</code></pre>

<p>Providers MAY use statuses other than &ldquo;open&rdquo; or &ldquo;closed&rdquo; to indicate other
statuses. Consumers who encounter an unknown or missing ping status SHOULD treat
it as &ldquo;closed&rdquo;.</p>

<h3><code>sticky</code></h3>

<p>The <code>sticky</code> field is a boolean indicating whether the post is marked as a
sticky post. Consumers typically display sticky posts before other posts in
collection views.</p>

<h3><code>post_thumbnail</code></h3>

<p>The <code>post_thumbnail</code> field is a Media entity.</p>

<h3><code>post_format</code></h3>

<p>The <code>post_format</code> field is a string with the post format. The post format
indicates how some meta fields should be displayed. For example, posts with the
&ldquo;link&rdquo; format may wish to display an extra link to a URL specified in a meta
field or emphasise a link in the post content.</p>

<pre><code>post-format = "standard" / "aside" / "gallery" / "image" / "link" / "status" / "quote" / "video" / "audio" / "chat"
</code></pre>

<p>Providers MUST NOT use post formats not specified by this specification, unless
specified in a subsequent version of the specification. Consumers MUST treat
unknown post formats as &ldquo;standard&rdquo;.</p>

<h3><code>terms</code></h3>

<p>The <code>terms</code> field is a Term collection.</p>

<h3><code>post_meta</code></h3>

<p>The <code>meta</code> field is a Metadata entity with metadata relating to the post.</p>

<h3><code>meta</code></h3>

<p>The <code>meta</code> field is a Entity Meta entity with metadata relating to the entity
representation.</p>

<h3>Example</h3>

<pre><code>{
    "ID": 1,
    "title": "Hello world!q",
    "status": "publish",
    "type": "post",
    "author": {
        "ID": 1,
        "name": "admin",
        "slug": "admin",
        "URL": "",
        "avatar": "http:\/\/0.gravatar.com\/avatar\/c57c8945079831fa3c19caef02e44614&amp;d=404&amp;r=G",
        "meta": {
            "links": {
                "self": "http:\/\/example.com\/wp-json\/users\/1",
                "archives": "http:\/\/example.com\/wp-json\/users\/1\/posts"
            }
        },
        "first_name": "",
        "last_name": ""
    },
    "content": "&lt;p&gt;Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!&lt;\/p&gt;\n",
    "parent": 0,
    "link": "http:\/\/example.com\/2013\/06\/02\/hello-world\/",
    "date": "2013-06-02T05:28:00+10:00",
    "modified": "2013-06-30T13:56:57+10:00",
    "format": "standard",
    "slug": "hello-world",
    "guid": "http:\/\/example.com\/?p=1",
    "excerpt": "",
    "menu_order": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "date_tz": "Australia\/Brisbane",
    "date_gmt": "2013-06-02T05:28:00+00:00",
    "modified_tz": "Australia\/Brisbane",
    "modified_gmt": "2013-06-30T03:56:57+00:00",
    "password": "",
    "post_meta": [
    ],
    "meta": {
        "links": {
            "self": "http:\/\/example.com\/wp-json\/posts\/1",
            "author": "http:\/\/example.com\/wp-json\/users\/1",
            "collection": "http:\/\/example.com\/wp-json\/posts",
            "replies": "http:\/\/example.com\/wp-json\/posts\/1\/comments",
            "version-history": "http:\/\/example.com\/wp-json\/posts\/1\/revisions"
        }
    },
    "featured_image": null,
    "terms": {
        "category": {
            "ID": 1,
            "name": "Uncategorized",
            "slug": "uncategorized",
            "parent": null,
            "count": 7,
            "meta": {
                "links": {
                    "collection": "http:\/\/example.com\/wp-json\/taxonomies\/category\/terms",
                    "self": "http:\/\/example.com\/wp-json\/taxonomies\/category\/terms\/1"
                }
            }
        }
    }
}
</code></pre>

<h2>Entity Meta</h2>

<p>The Entity Meta entity is a JSON object with custom metadata relating to the
representation of the parent entity.</p>

<p>The following properties are defined for the Entity Meta entity object:</p>

<h3><code>links</code></h3>

<p>The <code>links</code> field is a JSON object with hyperlinks to related entities. Each
item&rsquo;s key is a link relation as per the <a href="http://www.iana.org/assignments/link-relations/link-relations.xml">IANA Link Relations registry</a> with
the value of the item being the corresponding link URL.</p>

<p>Typical link relations are:</p>

<ul>
<li><code>self</code>: A URL pointing to the current entity&rsquo;s location.</li>
<li><code>up</code>: A URL pointing to the parent entity&rsquo;s location.</li>
<li><code>collection</code>: A URL pointing to a collection that the entity is a member of.</li>
</ul>


<h2>User</h2>

<p>The User entity is a JSON object with user properties. The following properties
are defined for the User entity object:</p>

<h3><code>ID</code></h3>

<p>The <code>ID</code> field is an integer with the user&rsquo;s ID.</p>

<h3><code>name</code></h3>

<p>The <code>name</code> field is a string with the user&rsquo;s display name.</p>

<h3><code>slug</code></h3>

<p>The <code>slug</code> field is a string with the user&rsquo;s slug.</p>

<h3><code>URL</code></h3>

<p>The <code>URL</code> field is a string with the URL to the author&rsquo;s site. This is typically
an external link of the author&rsquo;s choice.</p>

<h3><code>avatar</code></h3>

<p>The <code>avatar</code> field is a string with the URL to the author&rsquo;s avatar image.</p>

<p>Providers SHOULD ensure that for users without an avatar image, this field is
either zero-length or the URL returns a HTTP 404 error code on access. Consumers
MAY display a default avatar instead of a zero-length or URL which returns
a HTTP 404 error code.</p>

<h3><code>meta</code></h3>

<p>The <code>meta</code> field is a Entity Meta entity with metadata relating to the entity
representation.</p>

<h2>Metadata</h2>

<p>The Metadata entity is a JSON array with metadata fields. Each metadata field is
a JSON object with <code>id</code>, <code>key</code> and <code>value</code> fields.</p>

<h3><code>id</code></h3>

<p>The <code>id</code> field of the metadata field is a positive integer with the internal
metadata ID.</p>

<h3><code>key</code></h3>

<p>The <code>key</code> field of the metadata field is a string with the metadata field name.</p>

<h3><code>value</code></h3>

<p>The <code>value</code> field of the metadata field is a string with the metadata
field value.</p>

<h2>Comment</h2>

<p>The Comment entity is a JSON object with comment properties. The following
properties are defined for the Comment entity object:</p>

<h3><code>ID</code></h3>

<p>The <code>ID</code> field is an integer with the comment&rsquo;s ID.</p>

<h3><code>content</code></h3>

<p>The <code>content</code> field is a string with the comment&rsquo;s content.</p>

<h3><code>status</code></h3>

<p>The <code>status</code> field is a string with the comment&rsquo;s status. This field indicates
whether the comment is in the publishing process, or if it has been deleted or
marked as spam.</p>

<pre><code>comment-status = "hold" / "approved" / "spam" / "trash" / token
</code></pre>

<p>Providers MAY use other values to indicate other statuses. Consumers who
encounter an unknown or missing status SHOULD treat it as &ldquo;hold&rdquo;.</p>

<h3><code>type</code></h3>

<p>The <code>type</code> field is a string with the comment&rsquo;s type. This is usually one of the
following, but providers may provide additional values.</p>

<pre><code>comment-type = "comment" / "trackback" / "pingback" / token
</code></pre>

<p>Providers MAY use other values to indicate other types. Consumers who encounter
an unknown or missing status SHOULD treat it as &ldquo;comment&rdquo;.</p>

<h3><code>post</code></h3>

<p>The <code>post</code> field is an integer with the parent post for the comment, or a Post
entity describing the parent post. A literal zero indicates that the comment
does not have a parent post.</p>

<pre><code>comment-post-parent = "0" / 1*DIGIT
</code></pre>

<p>Consumers who encounter a missing post ID MUST treat it the same as a parent
post ID of 0.</p>

<h3><code>parent</code></h3>

<p>The <code>post</code> field is an integer with the parent comment, or a Comment entity
describing the parent comment. A literal zero indicates that the comment does
not have a parent comment.</p>

<pre><code>comment-parent = "0" / 1*DIGIT
</code></pre>

<p>Consumers who encounter a missing parent ID MUST treat it the same as a parent
comment ID of 0.</p>

<h3><code>author</code></h3>

<p>The <code>author</code> field is a User entity with the comment author&rsquo;s data, or a
User-like object for anonymous authors. The User-like object contains the
following properties:</p>

<h4><code>ID</code></h4>

<p>The <code>ID</code> property on the User-like object is always set to <code>0</code> for anonymous
authors.</p>

<h4><code>name</code></h4>

<p>The <code>name</code> property on the User-like object is a string with the author&rsquo;s name.</p>

<h4><code>URL</code></h4>

<p>The <code>URL</code> property on the User-like object is a string with the author&rsquo;s URL.</p>

<h4><code>avatar</code></h4>

<p>The <code>avatar</code> property on the User-like object is a string with the URL to the
author&rsquo;s avatar image.</p>

<p>This property should be treated the same as the avatar property on the
User entity.</p>

<h3><code>date</code>, <code>date_gmt</code></h3>

<p>The <code>date</code> and <code>date_gmt</code> fields are strings with the post&rsquo;s creation date and
time in the local time and UTC respectively. These fields follow the <a href="http://tools.ietf.org/html/rfc3339">RFC3339</a>
Section 5.6 datetime representation.</p>

<pre><code>date     = date-time
date_gmt = date-time
</code></pre>

<p>This field should be treated the same as the <code>date</code> and <code>date_gmt</code> properties on
a Post entity.</p>

<h3><code>date_tz</code>, <code>modified_tz</code></h3>

<p>The <code>date_tz</code> and <code>modified_tz</code> fields are strings with the timezone applying to
the <code>date</code> and <code>modified</code> fields respectively. The timezone is a <a href="https://en.wikipedia.org/wiki/Tz_database">Olsen zoneinfo
database</a> identifier. While the <code>date</code> field includes timezone offset
information, the <code>date_tz</code> field allows proper data operations across Daylight
Savings Time boundaries.</p>

<p>This field should be treated the same as the <code>date_tz</code> property on a
Post entity.</p>

<h2>Media</h2>

<p>The Media entity is a JSON object based on the Post entity. It contains all
properties of the Post entity, with the following additional properties defined:</p>

<h3><code>source</code></h3>

<p>The <code>source</code> field is a string with the URL of the entity&rsquo;s original file. For
image media, this is the source file that intermediate representations are
generated from. For non-image media, this is the attached media file itself.</p>

<h3><code>is_image</code></h3>

<p>The <code>is_image</code> field is a boolean which indicates whether the entity&rsquo;s
associated file should be handled as an image.</p>

<h3><code>attachment_meta</code></h3>

<p>The <code>attachment_meta</code> field is a Media Meta entity. If the file is not an image
(as indicated by the <code>is_image</code> field), this is an empty JSON object.</p>

<h2>Media Meta</h2>

<p>The Media Meta entity is a JSON object with properties relating to the
associated Media entity. The following properties are defined for the entity:</p>

<h3><code>width</code></h3>

<p>The <code>width</code> field is an integer with the original file&rsquo;s width in pixels.</p>

<h3><code>height</code></h3>

<p>The <code>height</code> field is an integer with the original file&rsquo;s height in pixels.</p>

<h3><code>file</code></h3>

<p>The <code>file</code> field is a string with the path to the original file, relative to the
site&rsquo;s upload directory.</p>

<h3><code>sizes</code></h3>

<p>The <code>sizes</code> field is a JSON object mapping intermediate image sizes to image
data objects. The key of each item is the size of the intermediate image as an
internal string representation. The value of each item has the following
properties defined.</p>

<ul>
<li><code>file</code>: The filename of the intermediate file, relative to the directory of
the original file.</li>
<li><code>width</code>: The width of the intermediate file in pixels.</li>
<li><code>height</code>: The height of the intermediate file in pixels.</li>
<li><code>mime-type</code>: The MIME type of the intermediate file.</li>
<li><code>url</code>: The full URL to the intermediate file.</li>
</ul>


<h3><code>image_meta</code></h3>

<p>The <code>image_meta</code> field is a JSON object mapping image meta properties to their
values. This data is taken from the EXIF data on the original image. The
following properties are defined.</p>

<ul>
<li><code>aperture</code>: The aperture used to create the original image as a decimal number
(with two decimal places).</li>
<li><code>credit</code>: Credit for the original image.</li>
<li><code>camera</code>: The camera used to create the original image.</li>
<li><code>created_timestamp</code>: When the file was created, as a Unix timestamp.</li>
<li><code>copyright</code>: Copyright for the original image.</li>
<li><code>focal_length</code>: The focal length used to create the original image as a
decimal string.</li>
<li><code>iso</code>: The ISO used to create the original image.</li>
<li><code>shutter_speed</code>: The shutter speed used to create the original image, as a
decimal string.</li>
<li><code>title</code>: The original title of the image.</li>
</ul>


<h1>Documents</h1>

<h2>Index</h2>

<p>The Index document is the root endpoint for the API server and describes the
contents and abilities of the API server.</p>

<h3>Body</h3>

<p>The body of an Index document is an Index entity.</p>

<h3>Example</h3>

<pre><code>{
    "name":"My WordPress Site",
    "description":"Just another WordPress site",
    "URL":"http:\/\/example.com",
    "routes": {
        "\/": {
            "supports": [ "HEAD", "GET" ]
        },
        "\/posts": {
            "supports": [ "HEAD", "GET", "POST" ],
            "accepts_json": true
        },
        "\/posts\/&lt;id&gt;": {
            "supports": [ "HEAD", "GET", "POST", "PUT", "PATCH", "DELETE" ]
        },
        "\/posts\/&lt;id&gt;\/revisions": {
            "supports": [ "HEAD", "GET" ]
        },
        "\/posts\/&lt;id&gt;\/comments": {
            "supports": [ "HEAD", "GET", "POST" ],
            "accepts_json":true
        }
    },
    "meta": {
        "links": {
            "help":"http:\/\/codex.wordpress.org\/JSON_API"
        }
    }
}
</code></pre>

<h2>Post</h2>

<p>A Post document is defined as the representation of a post item, analogous to an
Atom item.</p>

<h3>Headers</h3>

<p>The following headers are sent when a Post is the main entity:</p>

<ul>
<li><code>Link</code>:

<ul>
<li><code>rel="alternate"; type=text/html</code>: The permalink for the Post</li>
<li><code>rel="collection"</code>: The endpoint of the Post Collection the Post is
contained in</li>
<li><code>rel="replies"</code>: The endpoint of the associated Comment Collection</li>
<li><code>rel="version-history"</code>: The endpoint of the Post Collection containing
the revisions of the Post</li>
</ul>
</li>
</ul>


<h3>Body</h3>

<p>The body of a Post document is a Post entity.</p>

<h3>Example</h3>

<pre><code>HTTP/1.1 200 OK
Date: Mon, 07 Jan 2013 03:35:14 GMT
Last-Modified: Mon, 07 Jan 2013 03:35:14 GMT
Link: &lt;http://localhost/wptrunk/?p=1&gt;; rel="alternate"; type=text/html
Link: &lt;http://localhost/wptrunk/wp-json/users/1&gt;; rel="author"
Link: &lt;http://localhost/wptrunk/wp-json/posts&gt;; rel="collection"
Link: &lt;http://localhost/wptrunk/wp-json/posts/158/comments&gt;; rel="replies"
Link: &lt;http://localhost/wptrunk/wp-json/posts/158/revisions&gt;; rel="version-history"
Content-Type: application/json; charset=UTF-8

{
    "ID":158,
    "title":"This is a test!",
    "status":"publish",
    "type":"post",
    "author":{
        "ID":1,
        "name":"admin",
        "slug":"admin",
        "URL":"",
        "avatar":"http:\/\/0.gravatar.com\/avatar\/c57c8945079831fa3c19caef02e44614&amp;d=404&amp;r=G",
        "meta":{
            "links":{
                "self":"http:\/\/localhost\/wptrunk\/wp-json\/users\/1",
                "archives":"http:\/\/localhost\/wptrunk\/wp-json\/users\/1\/posts"
            }
        }
    },
    "content":"Hello.\r\n\r\nHah.",
    "parent":0,
    "link":"http:\/\/localhost\/wptrunk\/158\/this-is-a-test\/",
    "date":"2013-01-07T13:35:14+10:00",
    "modified":"2013-01-07T13:49:40+10:00",
    "format":"standard",
    "slug":"this-is-a-test",
    "guid":"http:\/\/localhost\/wptrunk\/?p=158",
    "excerpt":"",
    "menu_order":0,
    "comment_status":"open",
    "ping_status":"open",
    "sticky":false,
    "date_tz":"Australia\/Brisbane",
    "date_gmt":"2013-01-07T03:35:14+00:00",
    "modified_tz":"Australia\/Brisbane",
    "modified_gmt":"2013-01-07T03:49:40+00:00",
    "post_thumbnail":[],
    "terms":{
        "category":{
            "ID":1,
            "name":"Uncategorized",
            "slug":"uncategorized",
            "group":0,
            "parent":0,
            "count":4,
            "meta":{
                "links":{
                    "collection":"http:\/\/localhost\/wptrunk\/wp-json\/taxonomy\/category",
                    "self":"http:\/\/localhost\/wptrunk\/wp-json\/taxonomy\/category\/terms\/1"
                }
            }
        }
    },
    "post_meta":[],
    "meta":{
        "links":{
            "self":"http:\/\/localhost\/wptrunk\/wp-json\/posts\/158",
            "author":"http:\/\/localhost\/wptrunk\/wp-json\/users\/1",
            "collection":"http:\/\/localhost\/wptrunk\/wp-json\/posts",
            "replies":"http:\/\/localhost\/wptrunk\/wp-json\/posts\/158\/comments",
            "version-history":"http:\/\/localhost\/wptrunk\/wp-json\/posts\/158\/revisions"
        }
    }
}
</code></pre>

<h2>Post Collection</h2>

<p>A Post Collection document is defined as a collection of Post entities.</p>

<h3>Headers</h3>

<p>The following headers are sent when a Post Collection is the main entity:</p>

<ul>
<li><code>Link</code>:

<ul>
<li><code>rel="item"</code> - Each item in the collection has a corresponding Link header
containing the location of the endpoint for that resource.</li>
</ul>
</li>
</ul>


<h3>Body</h3>

<p>The Post Collection document is a JSON array of Post entities.</p>

<h2>User</h2>

<p>The User document describes a member of the site.</p>

<h3>Body</h3>

<p>The body of a User document is a User entity.</p>

<h1>Appendix A: JSON Schema</h1>

<p>The JSON Schema describing the entities in this document is available in
schema.json.</p>
