<h1>Working with Posts</h1>

<p>Back in the <a href="getting-started.md">Getting Started</a> guide we used posts to demonstrate how to work
with the API, but only touched on some of the details. Let&rsquo;s take a more
detailed look at the Post API.</p>

<h2>Before We Start</h2>

<p>This guide assumes you have a basic knowledge of the API, as well as the
prerequisites noted in the <a href="getting-started.md">Getting Started</a> guide. If you haven&rsquo;t already, make
sure you read that guide to ensure you know the basics.</p>

<p>This guide also assumes that you know how to send requests given how to use
them, so the examples will be HTTP requests. I recommend reading the cURL manual
or using a higher level tool if you don&rsquo;t know how to wrangle cURL.</p>

<p>The examples also pretend that your JSON base URL (<code>wp-json</code> in the main WP
directory) is located at <code>/</code>, which is probably not the case. For example, if
your base URL is <code>http://example.com/wp-json</code> and the example request is
<code>GET /posts</code>, you should should actually send the following:</p>

<pre><code>GET /wp-json/posts HTTP/1.1
Host: example.com
</code></pre>

<p>Higher level HTTP clients can usually handle this for you.</p>

<h2>Post Entities and Collections</h2>

<p>As mentioned previously, we can send a GET request to the main posts route to
get posts:</p>

<pre><code>GET /posts HTTP/1.1
</code></pre>

<p>This returns a list of Post objects. From a technical point of view, each object
is called an Entity (in this case, Post Entities), while the endpoint returns a
Collection (here, a Post Collection).</p>

<p>Post Entities have certain <a href="../schema.md">defined properties</a>, although which are set
depend on the context. For example, with embedded entities certain properties
may have different values; while the post parent entity is embedded, the parent
field inside this is only the ID.</p>

<h2>Collection Views</h2>

<p>Post Collections can be modified by using various query string parameters. One
that you may already know is the <code>page</code> parameter. This parameter is used for
pagination, and while you can set it manually, it&rsquo;s also sent via the Link
headers to point you to the next and previous pages. A <code>context</code> parameter is
also available that we&rsquo;ll talk about later.</p>

<p>You can also change the post type via the <code>type</code> parameter. It&rsquo;s recommended
that you check the <a href="#custom-post-types">available post types</a> before doing this,
as plugins may change what&rsquo;s available.</p>

<p>The last parameter is the <code>filter</code> parameter. This gives you full access to the
<a href="http://codex.wordpress.org/Class_Reference/WP_Query">WP_Query</a> parameters, to alter the query to your liking. Depending on the
level of access you have, not all parameters will be available, so check the
<a href="../schema.md">schema</a> for the available parameters. A good assumption to make is that
anything you can put in a query on the site itself (such as <code>?s=...</code> for
searches) will be available. You can specify filter parameters in a request
using <a href="../compatibility.md#inputting-data-as-an-array">array-style URL formatting</a>.</p>

<h2>Creating and Editing Posts</h2>

<p>The main posts route also has a creation endpoint:</p>

<pre><code>POST /posts HTTP/1.1
Content-Type: application/json; charset=utf-8

{
    "title": "My Post Title"
}
</code></pre>

<p>Here we send a Post entity that we&rsquo;ve created locally to the server. These Post
entities are the same as Post entities served up by the server, but excluding
some fields which are immutable. For example, the ID and timezone fields cannot
be changed.</p>

<p>When editing posts, it&rsquo;s helpful to pass the <code>edit</code> context along with your
request. This gives you extra data useful for editing, such as the unfiltered
content and title. This is not required, however it is recommended, as the
normal content and title fields have been filtered to correct HTML (such as via
the <code>wpautop</code> function), making them less than optimal for editing.</p>

<p>You can also pass an <code>If-Unmodified-Since</code> header when editing posts with the
previous modification date to ensure that you don&rsquo;t accidentally overwrite edits
made in the meantime. Note that while dates in the <code>modified</code> field of the post
are in <a href="http://tools.ietf.org/html/rfc3339">RFC3339</a> format, this header requires the use of <a href="http://tools.ietf.org/html/rfc1123">RFC1123</a> (similar
to <a href="http://tools.ietf.org/html/rfc822">RFC822</a>). (Sorry about that, but the HTTP standard requires it.)</p>

<h2>Custom Post Types</h2>

<p>Custom post types can be queried via the main post routes (<code>/posts</code> and
children) when they have been made public. The type can be set via the <code>type</code>
query parameter.</p>

<p>Before working with custom post types, you should first check that the API has
support for the post type that you want to work with. This data is available via
the read-only APIs at <code>/posts/types</code>:</p>

<pre><code>GET /posts/types HTTP/1.1
</code></pre>

<p>This should return a list of the available types:</p>

<pre><code>{
    "post": {
        "name": "Posts",
        "slug": "post",
        "description": "",
        "labels": {
            "name": "Posts",
            "singular_name": "Post",
            "add_new": "Add New",
            "add_new_item": "Add New Post",
            "edit_item": "Edit Post",
            "new_item": "New Post",
            "view_item": "View Post",
            "search_items": "Search Posts",
            "not_found": "No posts found.",
            "not_found_in_trash": "No posts found in Trash.",
            "parent_item_colon": null,
            "all_items": "All Posts",
            "menu_name": "Posts",
            "name_admin_bar": "Post"
        },
        "queryable": true,
        "searchable": true,
        "hierarchical": false,
        "meta": {
            "links": {
                "self": "http:\/\/example.com\/wp-json\/posts\/types\/post",
                "archives": "http:\/\/example.com\/wp-json\/posts"
            }
        }
    },
    "page": {
        "name": "Pages",
        "slug": "page",
        "description": "",
        "labels": {
            "name": "Pages",
            "singular_name": "Page",
            "add_new": "Add New",
            "add_new_item": "Add New Page",
            "edit_item": "Edit Page",
            "new_item": "New Page",
            "view_item": "View Page",
            "search_items": "Search Pages",
            "not_found": "No pages found.",
            "not_found_in_trash": "No pages found in Trash.",
            "parent_item_colon": "Parent Page:",
            "all_items": "All Pages",
            "menu_name": "Pages",
            "name_admin_bar": "Page"
        },
        "queryable": false,
        "searchable": true,
        "hierarchical": true,
        "meta": {
            "links": {
                "self": "http:\/\/example.com\/wp-json\/posts\/types\/page"
            }
        }
    },
    "attachment": {
        "name": "Media",
        "slug": "attachment",
        "description": "",
        "labels": {
            "name": "Media",
            "singular_name": "Media",
            "add_new": "Add New",
            "add_new_item": "Add New Post",
            "edit_item": "Edit Media",
            "new_item": "New Post",
            "view_item": "View Attachment Page",
            "search_items": "Search Posts",
            "not_found": "No posts found.",
            "not_found_in_trash": "No posts found in Trash.",
            "parent_item_colon": null,
            "all_items": "Media",
            "menu_name": "Media",
            "name_admin_bar": "Media"
        },
        "queryable": true,
        "searchable": true,
        "hierarchical": false,
        "meta": {
            "links": {
                "self": "http:\/\/example.com\/wp-json\/posts\/types\/attachment",
                "archives": "http:\/\/example.com\/wp-json\/posts?type=attachment"
            }
        }
    }
}
</code></pre>

<p>The <code>meta.links.archives</code> value gives a nicer way to access given post type
archives for HATEOAS supporters and should always be used rather than manually
setting the <code>type</code> parameter directly, as CPTs may create their own route
structure instead. The <code>labels</code> fields should also always be used when
displaying the field, as these are run through WordPress' translations.</p>

<p>A similar API exists for post statuses at <code>/posts/statuses</code>:</p>

<pre><code>{
    "publish": {
        "name": "Published",
        "slug": "publish",
        "public": true,
        "protected": false,
        "private": false,
        "queryable": true,
        "show_in_list": true,
        "meta": {
            "archives": "http:\/\/example.com\/wp-json\/posts"
        }
    },
    "future": {
        "name": "Scheduled",
        "slug": "future",
        "public": false,
        "protected": true,
        "private": false,
        "queryable": false,
        "show_in_list": true,
        "meta": [
        ]
    },
    "draft": {
        "name": "Draft",
        "slug": "draft",
        "public": false,
        "protected": true,
        "private": false,
        "queryable": false,
        "show_in_list": true,
        "meta": [
        ]
    },
    "pending": {
        "name": "Pending",
        "slug": "pending",
        "public": false,
        "protected": true,
        "private": false,
        "queryable": false,
        "show_in_list": true,
        "meta": [
        ]
    },
    "private": {
        "name": "Private",
        "slug": "private",
        "public": false,
        "protected": false,
        "private": true,
        "queryable": false,
        "show_in_list": true,
        "meta": [
        ]
    }
}
</code></pre>

<h2>Next Steps</h2>

<p>You should now understand more advanced usage of the post-related APIs, and be
able to implement a fully compliant client for the API. You might now want to
take a look at the other APIs, or look at documentation on the specifics.</p>

<ul>
<li><a href="../schema.md">Schema</a>: Full documentation of every parameter for the APIs.</li>
<li><a href="extending.md">Extending the API</a>: Create your own API endpoints.</li>
</ul>

