<h1>Extending the API</h1>

<p>If you&rsquo;re a plugin author, you&rsquo;re probably itching to start adding functionality
to the API, including working with custom post types. I&rsquo;m here to guide you
through the process of extending the API with your data, and to remind you of
some important points.</p>

<h2>Before We Start</h2>

<p>This guide assumes that you have a fairly decent knowledge of the API already.
Before starting, make sure you&rsquo;ve read the <a href="getting-started.md">Getting Started</a> guide, and if you
want to work with custom post types, also read the <a href="working-with-posts.md">Working With Posts</a> guide.</p>

<p>You should also have a pretty good knowledge of working with actions and filters
in WordPress, as well as how plugins work in general.</p>

<h2>A Philosophy Lesson</h2>

<p>Right off the bat, let me just say that the REST API is not designed for every
use under the sun. Following the mantra of <a href="http://wordpress.org/about/philosophy/">Decisions, not Options</a>,
the API is opinionated and designed for the most common use case. You may find
that what you&rsquo;re trying to do doesn&rsquo;t fit in with the REST philosophy, and
<strong>that&rsquo;s fine</strong>; the low-level <code>admin-ajax.php</code> handler is
<a href="http://codex.wordpress.org/AJAX_in_Plugins">still available</a> for you to use in these scenarios, and you can
always roll your own API. An example of an API which doesn&rsquo;t fit with this API
is the heartbeat API; heartbeats aren&rsquo;t inherently designed around resources,
but events, so are a poor fit for a REST API.</p>

<p>That said, evaluate whether it&rsquo;s better to rethink your structure in terms of
resources, or to roll your own API. In most cases, thinking about data in terms
of resources rather than events or actions will not only fit better with the
API, but also make for cleaner and more maintainable code. For example, although
publishing a post is an action, it&rsquo;s equivalent to updating a post&rsquo;s status.</p>

<p>Keep in mind that since the REST API is opinionated, you will have to work with
the API&rsquo;s structure rather than rolling your own. The API is strict about how
you talk to the client, and you should always use the built-in handlers rather
than rolling your own. This strict enforcement of data handling helps to make
the API more pluggable and extensible, plus ensures that you play nice with
other plugins.</p>

<h2>Designing Your Entities</h2>

<p>The first step you should take in working with the API is to think about the
data that you&rsquo;re working with. You should always think about the data in terms
of the clients that you&rsquo;re working with; it&rsquo;s your job to handle translation
back and forth. While it might seem easier to just return internal objects
directly, this may expose private data, or data that&rsquo;s harder to handle for
clients; posts internally translate WordPress&rsquo;s date format (YYYY-MM-DD
HH:MM:SS) to the standard RFC3339 which can be parsed by most date parsers. It&rsquo;s
worth your time to consider issues like this which can save clients hours of
work on their end.</p>

<p>You should also consider how other plugins might extend your data, and design
for this. While things like <code>comment_status</code> could become a boolean field,
plugins might add custom statuses. On the other side of this, you should also
consider how these fields should be handled by clients. Custom field values that
a client doesn&rsquo;t understand should have a sensible default handling; for
example, the <code>comment_status</code> field is documented as the following:</p>

<blockquote><p>Providers MAY use statuses other than &ldquo;open&rdquo; or &ldquo;closed&rdquo; to indicate other
statuses. Consumers who encounter an unknown or missing comment status SHOULD
treat it as &ldquo;closed&rdquo;.</p></blockquote>

<p>This ensures that while clients may not know what a field value means, they can
still operate in a safe manner with the posts.</p>

<h2>Designing Your Routes and Endpoints</h2>

<p>After you&rsquo;ve decided what your entities should look like, it&rsquo;s time to think
about how you want clients to access and interact with the data. Picking your
routes is an important part of this, and should be oriented around your data.
Collections of Entities should be a base route, with individual Entities as
subroutes of this. The following is a good general rule:</p>

<ul>
<li><code>/&lt;object&gt;</code>:

<ul>
<li>GET returns a collection</li>
<li>POST adds a new item to the collection</li>
</ul>
</li>
<li><code>/&lt;object&gt;/&lt;id&gt;</code>:

<ul>
<li>GET returns an entity</li>
<li>PUT updates the entity</li>
<li>DELETE deletes the entity</li>
</ul>
</li>
</ul>


<p>(Again, this is the difference between routes and endpoints. There are two
routes here, but with two and three endpoints respectively. In general, a route
maps to an Entity/Collection, whereas an endpoint maps to an action on that
Entity/Collection.)</p>

<p>Since your data is in your plugin, you should also prefix your routes. For
example, routes for Hello Dolly might look something like:</p>

<ul>
<li><code>/hello-dolly</code>: Get all lines from Hello Dolly</li>
<li><code>/hello-dolly/&lt;n&gt;</code>: Get the nth line from Hello Dolly</li>
<li><code>/hello-dolly/random</code>: Get a random line from Hello Dolly</li>
</ul>


<p>If you&rsquo;re extending an existing route, you might want to use a flat style of
prefixing. Plugins like Akismet might implement something like:</p>

<ul>
<li><code>/posts/&lt;id&gt;/comments/akismet-recheck</code>: Recheck a post&rsquo;s comments for spam</li>
</ul>


<p>Note that this is somewhat against the previously discussed philosophy of
organising around resources; sometimes, you might want to have actions
available directly. These should always be a POST endpoint, since you&rsquo;re taking
action on the server in response. You should also avoid this where possible,
although there&rsquo;s no need to go overboard and create things like an Akismet Queue
resource just for a single action.</p>

<h2>Organizing Your Endpoints</h2>

<p>Now that you&rsquo;ve got a roadmap for everything you&rsquo;d like to implement, it&rsquo;s time
to start implementing it.</p>

<p>The recommended way to organize your endpoints is to group related endpoints
together into a class, and extend the base classes built into the API.
Post-related endpoints should look to extend the <code>WP_JSON_CustomPostType</code> class,
and use the same method naming as it, falling back to it for the post
preparation and request handling. This will automatically register all the post
methods for their endpoints.</p>

<p>Along these lines, keep your methods named as generically as possible; while
<code>MyPlugin_API_MyType::get_my_type_items()</code> might seem like a good name, it makes it
harder for other plugins to use; standardising on
<code>MyPlugin_API_MyType::get_posts()</code> with similar arguments to the parent is a
better idea and allows a nicer fall-through.</p>

<p>You should also aim to keep these related endpoints modular, and make liberal
use of filters for this. As an example of this, the built-in Posts API is
filtered by the Media API to add featured image-related data, ensuring that
there&rsquo;s no dependencies between the two and either can be enabled independently.</p>

<h2>Creating Your Endpoints</h2>

<p>Before you actually write the code, there&rsquo;s some important things to know about
how you write the code. (Remember the opinions I mentioned before? This
is them.)</p>

<p>Each endpoint should be written in the same way you&rsquo;d write an internal function
for your plugin. That is, they take parameters, and return data. They also
<em>never</em> read directly from request globals (such as <code>$_GET</code> or <code>$_POST</code>), but
instead read them via the parameters to your function. The REST API serving code
automatically maps GET and POST parameters to your function&rsquo;s parameters (along
with some special request-related parameters). This concept might seem a little
strange at first, but it helps to ensure pluggability and standardisation of
errors.</p>

<p>For example, an endpoint that takes a required <code>context</code> parameter, an optional
<code>type</code> parameter and uses the <code>X-WP-Example</code> header would look like this:</p>

<pre><code>function get_my_data( $context, $_headers, $type = 'my-default-value' ) {
    if ( isset( $_headers['X-WP-EXAMPLE'] ) ) {
        $my_header_value = $_headers['X-WP-EXAMPLE'];
    }

    if ( $type !== 'my-default-value' &amp;&amp; $type !== 'some-other-value' ) {
        return new WP_Error( 'myplugin_mydata_invalid_type', __( 'Invalid type.' ), array( 'status' =&gt; 400 ) );
    }

    // ...

    return array( /* ... */ );
}
</code></pre>

<p>Note that this is intentionally the same way you&rsquo;d write a function for internal
consumption; default parameter values are specified as default argument values,
errors are returned as a WP_Error object (with the special <code>status</code> field in the
data set to an appropriate HTTP status), and the data itself being returned from
the function.</p>

<p>The following special values are also available, and can be trusted (always
internal data, not overridable by clients):</p>

<ul>
<li><code>_method</code>: The requested HTTP method (<code>GET</code>, <code>HEAD</code>, <code>POST</code>, <code>PUT</code>, <code>DELETE</code>)</li>
<li><code>_route</code>: The route followed to your endpoint (<code>/posts/(?P&lt;id&gt;\d+)</code>)</li>
<li><code>_path</code>: The actual path that matches your route (<code>/posts/1</code>)</li>
<li><code>_headers</code>: An associative array of header names to values. Names are always
uppercased (HTTP header names are case-insensitive)</li>
<li><code>_files</code>: An associative array of upload file data, in the same format
as <code>$_FILES</code></li>
</ul>


<p>The special <code>data</code> parameter is also available if your endpoint is set to
receive JSON or raw data. Note that this can be given by clients either via the
HTTP body or via the <code>data</code> query parameter, which is intentional for backwards
compatibility. That is, the following requests are the same:</p>

<pre><code>Content-Type: application/json; charset=utf-8

{
    "foo": "bar",
    "hello": "dolly"
}

----

Content-Type: application/x-www-form-urlencoded

data[foo]=bar&amp;data[hello]=dolly
</code></pre>

<p>The return value of your endpoints should be an error object with the status
field set to an <a href="http://en.wikipedia.org/wiki/List_of_HTTP_status_codes">appropriate value</a>, or the relevant data.
The data itself can be any JSON-serializable value: an integer, string, array,
object or null. In practice, this is almost always an array (for Collections) or
an object (for Entities).</p>

<p>Return headers can be set by returning an instance of <code>WP_JSON_Response</code>, which
includes a <code>header()</code> method, as well as similar helper methods
(<code>set_status()</code>, <code>link_header()</code>, <code>query_navigation_headers()</code>) called on the
response object, but should <em>never</em> be set via the direct <code>header()</code> or
<code>status_header()</code> functions.</p>

<h2>Registering Your Endpoints</h2>

<p>Now that we&rsquo;ve done the bulk of the work, it&rsquo;s time to tell WordPress that we
have an API to register. If you&rsquo;re following the same structure as the API&rsquo;s
built-in types, your registration code should look something like this:</p>

<pre><code>function myplugin_api_init() {
    global $myplugin_api_mytype;

    $myplugin_api_mytype = new MyPlugin_API_MyType();
    add_filter( 'json_endpoints', array( $myplugin_api_mytype, 'register_routes' ) );
}
add_action( 'wp_json_server_before_serve', 'myplugin_api_init' );

class MyPlugin_API_MyType {
    public function register_routes( $routes ) {
        $routes['/myplugin/mytypeitems'] = array(
            array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
            array( array( $this, 'new_post'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
        );
        $routes['/myplugin/mytypeitems/(?P&lt;id&gt;\d+)'] = array(
            array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
            array( array( $this, 'edit_post'), WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ),
            array( array( $this, 'delete_post'), WP_JSON_Server::DELETABLE ),
        );

        // Add more custom routes here

        return $routes;
    }

    // ...
}
</code></pre>

<p>You will need to implement the <code>get_post</code>, <code>edit_post</code>, <code>get_posts</code>, and
<code>new_post</code> methods within your new class. Take a look at the <code>WP_JSON_Posts</code>
class to see examples of how these methods can be written.</p>

<p>Alternatively, use the custom post type base class, which will handle the
hooking and more for you:</p>

<pre><code>// main.php
function myplugin_api_init( $server ) {
    global $myplugin_api_mytype;

    require_once dirname( __FILE__ ) . '/class-myplugin-api-mytype.php';
    $myplugin_api_mytype = new MyPlugin_API_MyType( $server );
    $myplugin-&gt;register_filters();
}
add_action( 'wp_json_server_before_serve', 'myplugin_api_init' );

// class-myplugin-api-mytype.php
class MyPlugin_API_MyType extends WP_JSON_CustomPostType {
    protected $base = '/myplugin/mytypeitems';
    protected $type = 'myplugin-mytype';

    public function register_routes( $routes ) {
        $routes = parent::register_routes( $routes );
        // $routes = parent::register_revision_routes( $routes );
        // $routes = parent::register_comment_routes( $routes );

        // Add more custom routes here

        return $routes;
    }

    // ...
}
</code></pre>

<p>(Note that this CPT base class handles other things as well, including strict
post type checking and correcting URLs.)</p>

<p>It is important that this class lives in a separate file that is only included
on a WP API hook, as your plugin may load before the WP API plugin. If you get
errors about the <code>WP_JSON_CustomPostType</code> class not being loaded, this is
the reason.</p>

<p>The data passed in and returned by the <code>json_endpoints</code> filter should be in the
format of an array containing a map of route regular expression to endpoint
lists. An endpoint list is a potentially unlimited array of endpoints' data.
Endpoint data is a two-element array, with the first element being the endpoint
callback, and the second element specifying options. In other words, the format
is:</p>

<pre><code>$routes['regex expression'] = array(
    // one or more:
    array(
        'callback_function',
        0 // bitwise options flag
    )
);
</code></pre>

<p>(Where possible, use named parameters of the form <code>(?P&lt;name&gt;...)</code> for variables
in your route, as these will be automatically replaced with a more friendly
version in the index.)</p>

<p>The available bitwise options are:</p>

<ul>
<li><code>WP_JSON_Server::READABLE</code>: Read endpoint (responds to GET)</li>
<li><code>WP_JSON_Server::CREATABLE</code>: Creation endpoint (responds to POST)</li>
<li><code>WP_JSON_Server::EDITABLE</code>: Edit endpoint (responds to POST/PUT/PATCH)</li>
<li><code>WP_JSON_Server::DELETABLE</code>: Deletion endpoint (responds to DELETE)</li>
<li><code>WP_JSON_Server::ALLMETHODS</code>: Generic endpoint (responds to
GET/POST/PUT/PATCH/DELETE)</li>
<li><code>WP_JSON_Server::ACCEPT_RAW</code>: Accepts raw data in the HTTP body</li>
<li><code>WP_JSON_Server::ACCEPT_JSON</code>: Accepts JSON data in the HTTP body
(automatically deserialized by the server)</li>
<li><code>WP_JSON_Server::HIDDEN_ENDPOINT</code>: Hide the endpoint from the index</li>
</ul>


<p>Once you&rsquo;ve done this, you should now go and check that your route is appearing
in the index. Whip out a HTTP client and test out that your API endpoints are
working correctly.</p>

<h2>Next Steps</h2>

<p>You should now have a working API built with the REST API. Congratulations!
You now have a mastery of how the API works, so get out there and start
expanding your API. You should consider contributing back to the core API and
improving the built-in APIs.</p>

<ul>
<li><a href="../internals/philosophy.md">API Philosophy</a>: Learn more about the design of the API and the decisions
made in the core.</li>
<li><a href="../schema.md">Schema</a>: Read up on the schema for the core API and use it to inspire your
own entity design.</li>
<li><a href="../internals/implementation.md">Internal Implementation</a>: Learn about how the REST server works internally.</li>
</ul>

