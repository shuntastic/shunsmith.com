<h1>Getting Started</h1>

<p>Hi! If you&rsquo;re reading this, you&rsquo;re probably wondering how to get started using
the REST API. In that case, you&rsquo;ve come to the right place! I&rsquo;m about to show
you the basics of the API from the ground up.</p>

<p>(If you&rsquo;re in the wrong place, there&rsquo;s not much I can do to help. Sorry!)</p>

<h2>Before We Start</h2>

<p>To interact with the API, you&rsquo;re going to need some tools. The first of these
you need is a HTTP client/library. These examples use command-line cURL, but any
HTTP client would do here.</p>

<p>The next tool you need is a JSON parser, and generator if you&rsquo;re sending data.
I&rsquo;m going to do this by hand for the command-line examples, but it&rsquo;s recommended
to use a proper serializer/deserializer in your programming language of choice.
That said, it&rsquo;s not a bad idea to know how to read and write JSON by hand.</p>

<h2>Checking for the API</h2>

<p>Before you start using the API, it&rsquo;s a good idea to check that the site itself
supports the API. WordPress is flexible enough to allow disabling the API or
parts thereof, and plugins may add or change parts of the API, so it&rsquo;s always
good to double-check this.</p>

<p>The easiest way to check this is to send a HEAD request to any page on the site.
Any site with the API enabled will return a <code>Link</code> header pointing to the API,
with the link relation set to <code>https://github.com/WP-API/WP-API</code>.</p>

<p>My test site for this documentation is set up at <code>http://example.com/</code>, so we
can find the API by sending a HEAD request to this URL:</p>

<pre><code>curl -I http://example.com/
</code></pre>

<p>(Uppercase <code>-I</code> here sends a HEAD request rather than a GET request, since we
only care about the headers here. I&rsquo;ll strip some irrelevant headers for
this documentation.)</p>

<p>And we get back:</p>

<pre><code>HTTP/1.1 200 OK
X-Pingback: http://example.com/wp/xmlrpc.php
Link: &lt;http://example.com/wp-json&gt;; rel="https://github.com/WP-API/WP-API"
</code></pre>

<p>The <code>Link</code> header tells us that the base URL for the API is
<code>http://example.com/wp-json</code>. Any routes should be appended to this. For
example, the API index that we&rsquo;ll use in a second is available at the <code>/</code> route,
so we append this to the URL to get <code>http://example.com/wp-json/</code>
(note the trailing slash).</p>

<p>For sites without pretty permalinks enabled, this will return something like
<code>http://example.com/?json_route=</code> instead; the route should again be appended
directly as a string, giving the API index at <code>http://example.com/?json_route=/</code></p>

<p>(You can also &ldquo;discover&rdquo; the API by looking for the HTML <code>&lt;link&gt;</code> in the
<code>&lt;head&gt;</code> with the same link relation, or in the RSD along with other WordPress
API information.)</p>

<h2>Checking the API Index</h2>

<p>As our next command, let&rsquo;s go ahead and check the API index. The index tells us
what routes are available, and a short summary about the site.</p>

<p>As we discovered previously, the index is available at the site&rsquo;s address with
<code>/wp-json/</code> on the end. Let&rsquo;s fire off the request:</p>

<pre><code>curl -i http://example.com/wp-json/
</code></pre>

<p>(By the way, <code>-i</code> tells cURL that we want to see the headers as well. As before,
I&rsquo;ll strip some irrelevant ones for this documentation.)</p>

<p>And here&rsquo;s what we get back:</p>

<pre><code>HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8
</code></pre>

<p><code>json
{
    "name": "My WordPress Site",
    "description": "Just another WordPress site",
    "URL": "http:\/\/example.com",
    "routes": {
        "\/": {
            "supports": [
                "HEAD",
                "GET"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/"
            }
        },
        "\/posts": {
            "supports": [
                "HEAD",
                "GET",
                "POST"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/posts"
            },
            "accepts_json": true
        },
        "\/posts\/&lt;id&gt;": {
            "supports": [
                "HEAD",
                "GET",
                "POST",
                "PUT",
                "PATCH",
                "DELETE"
            ],
            "accepts_json": true
        },
        "\/posts\/&lt;id&gt;\/revisions": {
            "supports": [
                "HEAD",
                "GET"
            ]
        },
        "\/posts\/&lt;id&gt;\/comments": {
            "supports": [
                "HEAD",
                "GET",
                "POST"
            ],
            "accepts_json": true
        },
        "\/posts\/&lt;id&gt;\/comments\/&lt;comment&gt;": {
            "supports": [
                "HEAD",
                "GET",
                "POST",
                "PUT",
                "PATCH",
                "DELETE"
            ],
            "accepts_json": true
        },
        "\/posts\/types": {
            "supports": [
                "HEAD",
                "GET"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/posts\/types"
            }
        },
        "\/posts\/types\/&lt;type&gt;": {
            "supports": [
                "HEAD",
                "GET"
            ]
        },
        "\/posts\/statuses": {
            "supports": [
                "HEAD",
                "GET"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/posts\/statuses"
            }
        },
        "\/taxonomies": {
            "supports": [
                "HEAD",
                "GET"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/taxonomies"
            }
        },
        "\/taxonomies\/&lt;taxonomy&gt;": {
            "supports": [
                "HEAD",
                "GET"
            ]
        },
        "\/taxonomies\/&lt;taxonomy&gt;\/terms": {
            "supports": [
                "HEAD",
                "GET"
            ]
        },
        "\/taxonomies\/&lt;taxonomy&gt;\/terms\/&lt;term&gt;": {
            "supports": [
                "HEAD",
                "GET"
            ]
        },
        "\/users": {
            "supports": [
                "HEAD",
                "GET",
                "POST"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/users"
            },
            "accepts_json": true
        },
        "\/users\/me": {
            "supports": [
                "HEAD",
                "GET",
                "POST",
                "PUT",
                "PATCH",
                "DELETE"
            ],
            "meta": {
                "self": "http:\/\/example.com\/wp-json\/users\/me"
            }
        },
        "\/users\/&lt;user&gt;": {
            "supports": [
                "HEAD",
                "GET",
                "POST"
            ],
            "accepts_json": true
        }
    },
    "meta": {
        "links": {
            "help": "https:\/\/github.com\/WP-API\/WP-API",
            "profile": "https:\/\/raw.github.com\/rmccue\/WP-API\/master\/docs\/schema.json"
        }
    }
}
</code></p>

<p>Wow, that&rsquo;s a lot of data! Let&rsquo;s break it down.</p>

<p>First, let&rsquo;s look at the headers. We get a 200 status code back, indicating that
we can successfully get the index. Note that it&rsquo;s possible for sites to disable
the API, which would give us a 404 error. You can check the returned body to
find out if the API exists at all; a disabled site will give you a JSON object
with the <code>json_disabled</code> error code, while a site without the API at all will
probably give you an error from the theme.</p>

<p>Next, we can see the <code>name</code>, <code>description</code> and <code>URL</code> fields. These are intended
to be used if you want to show the site&rsquo;s name to users, such as in settings
dialogs.</p>

<p>The <code>routes</code> object contains the meat of the body. This object has a bunch of
keys with &ldquo;routes&rdquo; (templates for creating URLs) pointing to objects containing
data (this data tells you about the &ldquo;endpoint&rdquo;). Each of these objects has at
least a <code>supports</code> key that contains a list of the HTTP methods supported. The
<code>meta</code> key has a hyperlink to the route, while the <code>accepts_json</code> key will also
be set and be <code>true</code> if you can POST/PUT JSON directly to the endpoint.</p>

<p>You might notice that some of the routes have a <code>&lt;something&gt;</code> part in them. This
is a variable part, and it tells you that you can replace it with something,
such as an object ID.</p>

<h2>Routes vs Endpoints</h2>

<p>Quick note on terminology: a &ldquo;route&rdquo; is a URL that gets passed into the API;
these look like <code>/posts</code> or <code>/posts/2</code> and can be written in a generic form as
<code>/some/part/&lt;variable_part&gt;</code>. An &ldquo;endpoint&rdquo; on the other hand is the handler
that actually does something with your request.</p>

<p>For example, the <code>/posts</code> route has two endpoints: the Retrieve Posts endpoint
to handle GET requests, and the Create Post endpoint to handle POST requests.</p>

<h2>Meta Objects</h2>

<p>You&rsquo;ll also see in the response that we have a <code>meta</code> object at the top level,
plus a bunch of smaller ones for the routes. Meta objects are mappings of <a href="http://www.iana.org/assignments/link-relations/link-relations.xhtml">link
relations</a> to their corresponding URL. These URLs are usually internal URLs to
help you browse the API, and can be used by clients embracing the <a href="http://en.wikipedia.org/wiki/HATEOAS">HATEOAS</a>
methodology.</p>

<p>Here&rsquo;s some common relations you&rsquo;ll see:</p>

<ul>
<li><code>self</code>: The canonical URL for the resource. This is usually most helpful in
 embedded objects (post parent, author, etc)</li>
<li><code>collection</code>: The URL for the collection containing the resource. The endpoint
 at this route usually gives a list, of which, the current resource is a
 member (for example, <code>/posts/1</code> would have a collection of <code>/posts</code>)</li>
<li><code>archive</code>: The URL for the collection owned by this resource (for example,
 posts written by an author)</li>
<li><code>replies</code>: Responses to a resource (for example, comments on a post, replies
 to a comment)</li>
</ul>


<h2>Getting Posts</h2>

<p>Now that we understand some of the basics, let&rsquo;s have a look at the posts route.
All we need to do is send a GET request to the posts endpoint.</p>

<pre><code>curl -i http://example.com/wp-json/posts
</code></pre>

<p>And this time, we get (again trimming headers, you&rsquo;ll have more than this):</p>

<pre><code>HTTP/1.1 200 OK
Last-Modified: Wed, 31 Oct 2012 18:26:17 GMT
Link: &lt;http://example.com/wp-json/posts/1&gt;; rel="item"; title="Hello world!"
</code></pre>

<p><code>json
[
    {
        "ID": 1,
        "title": "Hello world!",
        "status": "publish",
        "type": "post",
        "author": {
            "ID": 1,
            "name": "admin",
            "slug": "admin",
            "URL": "",
            "avatar": "http:\/\/0.gravatar.com\/avatar\/c57c8945079831fa3c19caef02e44614&amp;d=404&amp;r=G",
            "meta": {
                "links": {
                    "self": "http:\/\/example.com\/wp-json\/users\/1",
                    "archives": "http:\/\/example.com\/wp-json\/users\/1\/posts"
                }
            }
        },
        "content": "Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!",
        "parent": 0,
        "link": "http:\/\/example.com\/2012\/10\/31\/hello-world\/",
        "date": "2012-10-31T18:26:17+10:00",
        "modified": "2012-10-31T18:26:17+10:00",
        "format": "standard",
        "terms": {
            "category": {
                "ID": 1,
                "name": "Uncategorized",
                "slug": "uncategorized",
                "group": 0,
                "parent": 0,
                "count": 1,
                "meta": {
                    "links": {
                        "collection": "http:\/\/example.com\/wp-json\/taxonomy\/category",
                        "self": "http:\/\/example.com\/wp-json\/taxonomy\/category\/terms\/1"
                    }
                }
            }
        },
        "meta": {
            "links": {
                "self": "http:\/\/example.com\/wp-json\/posts\/1",
                "author": "http:\/\/example.com\/wp-json\/users\/1",
                "collection": "http:\/\/example.com\/wp-json\/posts",
                "replies": "http:\/\/example.com\/wp-json\/posts\/1\/comments",
                "version-history": "http:\/\/example.com\/wp-json\/posts\/1\/revisions"
            }
        }
    }
]
</code></p>

<p>Hopefully this looks fairly self-explanatory. For a full look at all the fields
you can get back from this, take a look at <a href="working-with-posts.md">Working with Posts</a> or the
<a href="../schema.md">definition</a> for the Post entity.</p>

<p>You&rsquo;ll notice that in this case we&rsquo;re getting a list back with just a single
item, one Post. If you have two Posts here, you&rsquo;ll get back two, and so on, up
to 10. If there are more than 10, pagination will kick in and you&rsquo;ll have to
handle this in your client.</p>

<p>Pagination details are given in the HTTP headers from the endpoint. The
<code>X-WP-Total</code> header has the total number of posts available to you, while the
<code>X-WP-TotalPages</code> header has the number of pages. While you can build the page
URL manually, you should use the <code>next</code> and <code>prev</code> links from the <code>Link</code> header
where possible.</p>

<p>Example of pagination headers:</p>

<pre><code>X-WP-Total: 492
X-WP-TotalPages: 50
Link: &lt;http://example.com/wp-json/posts?page=4&gt;; rel="next",
 &lt;http://example.com/wp-json/posts?page=2&gt;; rel="prev"
</code></pre>

<p>If you want to grab a single post, you can instead send a GET request to the
post itself. You can grab the URL for this from the <code>meta.links.self</code> field, or
construct it yourself (<code>/posts/&lt;id&gt;</code>):</p>

<pre><code>curl -i http://example.com/wp-json/posts/1
</code></pre>

<h2>Editing and Creating Posts</h2>

<p>Just as we can use a GET request to get a post, we can use PUT to edit a post.
The easiest way to do this is to send a JSON body back to the server with just
the fields you want to change. <a href="../authentication.md">Authentication</a> is <strong>required</strong> to edit
posts. For example, to edit the title and the modification date:</p>

<p><code>json
{
    "title": "Hello Updated World!",
    "modified": "2013-04-01T14:00:00+10:00"
}
</code></p>

<p>Save the data as &ldquo;updated-post.json&rdquo;, then we can send this to the server with
the correct headers and authentication. We will provide our username and
password with HTTP Basic authentication, which requires the <a href="https://github.com/WP-API/Basic-Auth">Basic Auth plugin</a>
be installed:</p>

<pre><code>curl --data-binary "@updated-post.json" \
    -H "Content-Type: application/javascript" \
    --user admin:password \
    http://example.com/wp-json/posts/1
</code></pre>

<p>And we should get back a 200 status code, indicating that the post has been
updated, plus the updated Post in the body.</p>

<p>Note that there are some fields we can&rsquo;t update; ID is an obvious example, but
others like timezone fields cannot be updated either. Check the <a href="../schema.md">schema</a> for
more details on this.</p>

<p>Similarly to editing posts, you can create posts. <a href="../authentication.md">Authentication</a> is
<strong>required</strong> to create posts. We can use the same data from before, but this
time, we POST it to the main posts route. Again, we are providing our username
and password using HTTP Basic authentication which requires the
<a href="https://github.com/WP-API/Basic-Auth">Basic Auth plugin</a> be installed:</p>

<pre><code>curl --data-binary "@updated-post.json" \
    -H "Content-Type: application/javascript" \
    --user admin:password \
    http://example.com/wp-json/posts
</code></pre>

<p>We should get a similar response to the editing endpoint, but this time we get
a 201 Created status code, with a Location header telling us where to access the
post in future:</p>

<pre><code>HTTP/1.1 201 Created
Location: http://example.com/wp-json/posts/2
</code></pre>

<p>Finally, we can clean this post up and delete it by sending a DELETE request:</p>

<pre><code>curl -X DELETE --user admin:password http://example.com/wp-json/posts/2
</code></pre>

<p>In general, routes follow the same pattern:</p>

<ul>
<li><code>/&lt;object&gt;</code>:

<ul>
<li>GET returns a collection</li>
<li>POST adds a new item to the collection</li>
</ul>
</li>
<li><code>/&lt;object&gt;/&lt;id&gt;</code>:

<ul>
<li>GET returns an entity</li>
<li>PUT updates the entity</li>
<li>DELETE deletes the entity</li>
</ul>
</li>
</ul>


<p>Note that by convention, the plural form of <code>&lt;object&gt;</code> is used in the URL
(e.g. <code>posts</code> instead of <code>post</code>, <code>pages</code> instead of <code>page</code>).</p>

<h2>Next Steps</h2>

<p>You should now be able to understand the basics of accessing and creating data
via the API, plus have the ability to apply this to posts. We&rsquo;ve only really
touched on the post-related data so far, and there&rsquo;s plenty more to the API, so
get exploring!</p>

<ul>
<li><a href="working-with-posts.md">Working With Posts</a>: Learn more about Posts and their data</li>
<li><a href="../schema.md">Schema</a>: View technical information on all the available data</li>
<li><a href="../authentication.md">Authentication</a>: Explore authentication options</li>
</ul>

