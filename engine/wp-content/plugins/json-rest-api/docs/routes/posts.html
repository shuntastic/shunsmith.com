<h1>Posts</h1>

<h2>Create a Post</h2>

<pre><code>POST /posts
</code></pre>

<h3>Input</h3>

<p>The <code>data</code> parameter consists of the elements of the Post object to be
created.  This data can be submitted via a regular HTTP multipart body, with
the Post keys and values set to the <code>data</code> parameter, or through a direct JSON
body.</p>

<p>That is, the following are equivalent:</p>

<p>Content-Type: application/x-www-form-urlencoded</p>

<pre><code>data[title]=Hello%20World!&amp;data[content_raw]=Content&amp;data[excerpt_raw]=Excerpt
</code></pre>

<p>Content-Type: application/json</p>

<pre><code>{"title":"Hello World!","content_raw":"Content","excerpt_raw":"Excerpt"}
</code></pre>

<p>The <code>data</code> parameter should be an object containing the following key value
pairs:</p>

<ul>
<li><code>title</code> - Title of the post. (string) <strong><em>required</em></strong></li>
<li><code>content_raw</code> - Full text of the post. (string) <strong><em>required</em></strong></li>
<li><code>excerpt_raw</code> - Text for excerpt of the post. (string) <em>optional</em></li>
<li><code>name</code> - Slug of the post. (string) <em>optional</em></li>
<li><code>status</code> - Post status of the post: <code>draft</code>, <code>publish</code>, <code>pending</code>, <code>future</code>,
<code>private</code>, or any custom registered status.  If providing a status of
<code>future</code>, you must specify a <code>date</code> in order for the post to be published as
expected.  Default is <code>draft</code>. (string) <em>optional</em></li>
<li><code>type</code> - Post type of the post: <code>post</code>, <code>page</code>, <code>link</code>, <code>nav_menu_item</code>, or
a any custom registered type.  Default is <code>post</code>. (string) <em>optional</em></li>
<li><code>date</code> - Date and time the post was, or should be, published in local time.
Date should be an RFC3339 timestamp](http://tools.ietf.org/html/rfc3339).
Example: 2014-01-01T12:20:52Z.  Default is the local date and time. (string)
<em>optional</em></li>
<li><code>date_gmt</code> - Date and time the post was, or should be, published in UTC time.
Date should be an <a href="http://tools.ietf.org/html/rfc3339">RFC3339 timestamp</a>.
Example: 201401-01T12:20:52Z.  Default is the current GMT date and time.
(string) <em>optional</em></li>
<li><code>author</code> - Author of the post.  Author can be provided as a string of the
author&rsquo;s ID or as the User object of the author.  Default is current user.
(object | string) <em>optional</em></li>
<li><code>password</code> - Password for protecting the post.  Default is empty string.
(string) <em>optional</em></li>
<li><code>post_parent</code> - Post ID of the post parent.  Default is 0. (integer)
<em>optional</em></li>
<li><code>post_format</code> - Format of the post.  Default is <code>standard</code>. (string)
<em>optional</em></li>
<li><code>menu_order</code> - The order in which posts specified as the <code>page</code> type should
appear in supported menus.  Default 0. (integer) <em>optional</em></li>
<li><code>comment_status</code> - Comment status for the post: <code>open</code> or <code>closed</code>.
Indicates whether users can submit comments to the post.  Default is the
option &lsquo;default_comment_status&rsquo;, or &lsquo;closed&rsquo;. (string) <em>optional</em></li>
<li><code>ping_status</code> - Ping status for the post: <code>open</code> or <code>closed</code>.  Indicates
whether users can submit pingbacks or trackbacks to the post.  Default is the
option &lsquo;default_ping_status&rsquo;. (string) <em>optional</em></li>
<li><code>sticky</code> - Sticky status for the post: <code>true</code> or <code>false</code>.  Default is
<code>false</code>.  (boolean) <em>optional</em></li>
<li><code>post_meta</code> - Post meta entries of the post.  Post meta should be an array
of one or more Meta objects for each post meta entry.  See the Create Meta
for a Post endpoint for the key value pairs.  (array) <em>optional</em></li>
</ul>


<h3>Response</h3>

<p>On a successful creation, a 201 Created status is given, indicating that the
post has been created. The post is available canonically from the URL specified
in the Location header.</p>

<p>The new Post entity is also returned in the body for convienience.</p>

<h2>Retrieve Posts</h2>

<p>The Posts endpoint returns a Post Collection containing a subset of the site&rsquo;s
posts.</p>

<pre><code>GET /posts
</code></pre>

<h3>Input</h3>

<h4><code>filter</code></h4>

<p>The <code>filter</code> parameter controls the query parameters.  It is essentially a subset of the parameters available to <a href="http://codex.wordpress.org/Class_Reference/WP_Query"><code>WP_Query</code></a>.</p>

<p>The parameter should be an array of the following key/value pairs:</p>

<ul>
<li><code>post_status</code> - Comma-separated list of <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Status_Parameters">status
values</a>.
Default is &ldquo;publish&rdquo;. (string)</li>
<li><code>numberposts</code> - Number of posts to retrieve, use <code>-1</code> for all posts. Default
is set by the site. (integer)</li>
<li><code>offset</code> - Number of posts to skip. Default is 0. (integer)</li>
<li><code>orderby</code> - Parameter to search by, as per <a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters">WP Query</a>.  Default is
&ldquo;date&rdquo;. (string)</li>
<li><code>order</code> - Order to sort by. Default is &ldquo;DESC&rdquo;. (string, &ldquo;ASC&rdquo; or &ldquo;DESC&rdquo;)</li>
<li><code>s</code> - Keyword to search for. (string)</li>
</ul>


<h4><code>context</code></h4>

<p>The <code>context</code> parameter controls the format of the data to return. See the
Retrieve a Post endpoint for available contexts.</p>

<p>Default is &ldquo;view&rdquo;. (string)</p>

<h4><code>type</code></h4>

<p>The <code>type</code> parameter specifies the post type to retrieve. This can either be a
string or an array of types.</p>

<p>Note that arrays are specified using the <code>[]</code> URL syntax. e.g.</p>

<p><code>
GET /posts?type[]=post&amp;type[]=page
</code></p>

<p>Default is &ldquo;post&rdquo;. (string)</p>

<h3>Response</h3>

<p>The response is a Post Collection document containing the requested Posts if
available.</p>

<h2>Retrieve a Post</h2>

<pre><code>GET /posts/&lt;id&gt;
</code></pre>

<h3>Input</h3>

<h4><code>context</code></h4>

<p>The <code>context</code> parameter controls the format of the data to return.  The
following contexts are available:</p>

<ul>
<li><code>view</code>: The default context. Gives the normal User entity.</li>
<li><code>edit</code>: Context used for extra fields relevant to updating a user. Includes
the <code>title_raw</code>, <code>content_raw</code>, <code>guid_raw</code> and <code>post_meta</code> fields, suitable
for editing the post.</li>
<li><code>parent</code>: Context used when embedding the response inside another (e.g. post
author). This is intended as a minimal subset of the user data to reduce
response size. Returns the <code>parent</code> field as an ID, rather than an embedded
post, to ensure we don&rsquo;t traverse the entire post hierarchy.</li>
</ul>


<h3>Response</h3>

<p>The response is a Post entity containing the requested Post if available. The
fields available on the Post depend on the <code>context</code> parameter.</p>

<h2>Edit a Post</h2>

<pre><code>PUT /posts/&lt;id&gt;
</code></pre>

<p>For compatibility reasons, this endpoint also accepts the POST and PATCH
methods. Both of these methods have the same behaviour as using PUT. It is
recommended to use PUT if available to fit with REST convention.</p>

<h3>Input</h3>

<p>The <code>data</code> parameter consists of Post ID and the elements of the Post object
to be modified.  This data can be submitted via a regular HTTP multipart body,
with the Post keys and values set to the <code>data</code> parameter, or through a direct
JSON body.  See the Create Post endpoint for an example.</p>

<p>The <code>data</code> parameter should be an object containing the following key value
pairs:</p>

<ul>
<li><code>ID</code> - Unique ID of the post. (integer) <strong><em>required</em></strong></li>
<li><code>title</code> - Title of the post. (string) <strong><em>required</em></strong></li>
<li><code>content_raw</code> - Full text of the post. (string) <strong><em>required</em></strong></li>
<li><code>excerpt_raw</code> - Text for excerpt of the post. (string) <em>optional</em></li>
<li><code>name</code> - Slug of the post. (string) <em>optional</em></li>
<li><code>status</code> - Post status of the post: <code>draft</code>, <code>publish</code>, <code>pending</code>, <code>future</code>,
<code>private</code>, or any custom registered status.  If providing a status of
<code>future</code>, you must specify a <code>date</code> in order for the post to be published as
expected.  Default is <code>draft</code>. (string) <em>optional</em></li>
<li><code>type</code> - Post type of the post: <code>post</code>, <code>page</code>, <code>link</code>, <code>nav_menu_item</code>, or
a any custom registered type.  Default is <code>post</code>. (string) <em>optional</em></li>
<li><code>date</code> - Date and time the post was, or should be, published in local time.
Date should be an RFC3339 timestamp](http://tools.ietf.org/html/rfc3339).
Example: 2014-01-01T12:20:52Z.  Default is the local date and time. (string)
<em>optional</em></li>
<li><code>date_gmt</code> - Date and time the post was, or should be, published in UTC time.
Date should be an <a href="http://tools.ietf.org/html/rfc3339">RFC3339 timestamp</a>.
Example: 201401-01T12:20:52Z.  Default is the current GMT date and time.
(string) <em>optional</em></li>
<li><code>author</code> - Author of the post.  Author can be provided as a string of the
author&rsquo;s ID or as the User object of the author.  Default is current user.
(object | string) <em>optional</em></li>
<li><code>password</code> - Password for protecting the post.  Default is empty string.
(string) <em>optional</em></li>
<li><code>post_parent</code> - Post ID of the post parent.  Default is 0. (integer)
<em>optional</em></li>
<li><code>post_format</code> - Format of the post.  Default is <code>standard</code>. (string)
<em>optional</em></li>
<li><code>menu_order</code> - The order in which posts specified as the <code>page</code> type should
appear in supported menus.  Default 0. (integer) <em>optional</em></li>
<li><code>comment_status</code> - Comment status for the post: <code>open</code> or <code>closed</code>.
Indicates whether users can submit comments to the post.  Default is the
option &lsquo;default_comment_status&rsquo;, or &lsquo;closed&rsquo;. (string) <em>optional</em></li>
<li><code>ping_status</code> - Ping status for the post: <code>open</code> or <code>closed</code>.  Indicates
whether users can submit pingbacks or trackbacks to the post.  Default is the
option &lsquo;default_ping_status&rsquo;. (string) <em>optional</em></li>
<li><code>sticky</code> - Sticky status for the post: <code>true</code> or <code>false</code>.  Default is
<code>false</code>.  (boolean) <em>optional</em></li>
<li><code>post_meta</code> - Post meta entries of the post.  Post meta should be an array
of one or more Meta objects for each post meta entry.  See the Edit Meta
for a Post endpoint for the key value pairs.  (array) <em>optional</em></li>
</ul>


<h3>Response</h3>

<p>On a successful update, a 200 OK status is given, indicating the post has been
updated. The updated Post entity is returned in the body.</p>

<h2>Delete a Post</h2>

<pre><code>DELETE /posts/&lt;id&gt;
</code></pre>

<h3>Input</h3>

<h4><code>force</code></h4>

<p>The <code>force</code> parameter controls whether the post is permanently deleted or not.
By default, this is set to false, indicating that the post will be sent to an
intermediate storage (such as the trash) allowing it to be restored later. If
set to true, the post will not be able to be restored by the user.</p>

<p>Default is false. (boolean)</p>

<h3>Response</h3>

<p>On successful deletion, a 202 Accepted status code will be returned, indicating
that the post has been moved to the trash for permanent deletion at a
later date.</p>

<p>If force was set to true, a 200 OK status code will be returned instead,
indicating that the post has been permanently deleted.</p>

<h2>Create Meta for a Post</h2>

<pre><code>POST /posts/&lt;id&gt;/meta
</code></pre>

<h3>Input</h3>

<p>The supplied data should be a Meta object. This data can be submitted via a
regular HTTP multipart body, with the Meta key and value set with the <code>data</code>
parameter, or through a direct JSON body.</p>

<p>The <code>data</code> parameter should be an object containing the following key value
pairs:</p>

<ul>
<li><code>key</code> - The post meta key to be created. (string) <em>required</em></li>
<li><code>value</code> - The post meta value for the key provided. (string) <em>required</em></li>
</ul>


<h3>Response</h3>

<p>On a successful creation, a 201 Created status is given, indicating that the
Meta has been created.  The post meta is available canonically from the URL
specified in the Location header.</p>

<p>The new Meta entity is also returned in the body for convienience.</p>

<h2>Retrieve Meta for a Post</h2>

<pre><code>GET /posts/&lt;id&gt;/meta
</code></pre>

<h3>Response</h3>

<p>The response is a Meta entity containing all the post_meta for the specified
Post if available.</p>

<h2>Retrieve a Meta for a Post</h2>

<pre><code>GET /posts/&lt;id&gt;/meta/&lt;mid&gt;
</code></pre>

<h3>Response</h3>

<p>The response a Meta entity containing the post_meta for the specified Meta and
Post if available.</p>

<h2>Edit a Meta for a Post</h2>

<pre><code>PUT /posts/&lt;id&gt;/meta/&lt;mid&gt;
</code></pre>

<h3>Input</h3>

<p>The supplied data should be a Meta object. This data can be submitted via a
regular HTTP multipart body, with the Meta key and value set with the <code>data</code>
parameter, or through a direct JSON body.</p>

<p>The <code>data</code> parameter should be an array containing the following key value pairs:</p>

<ul>
<li><code>key</code> - The post meta key to be updated. (string) <em>required</em></li>
<li><code>value</code> - The post meta value for the key provided. (string) <em>required</em></li>
</ul>


<h3>Response</h3>

<p>On a successful update, a 200 OK status is given, indicating the post_meta has
been updated. The updated Meta entity is returned in the body.</p>

<h2>Delete a Meta for a Post</h2>

<pre><code>DELETE /posts/&lt;id&gt;/meta/&lt;mid&gt;
</code></pre>

<h3>Response</h3>

<p>On successful deletion, a 200 OK status code will be returned, indicating
that the post_meta has been permanently deleted.</p>
