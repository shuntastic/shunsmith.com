<?php
/*
Single Post Template: Story
* @package required+ Foundation
* @since required+ Foundation 0.2.0
*/
?>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>
<body <?php body_class(); ?> style="background:transparent;">
	<!-- Row for main content area -->
<div id="content" class="row">
	<div class="project">
				<?php while ( have_posts() ) : the_post(); ?>
                        	<?php 
								$currID = $post->ID;

								$projectDescription = simple_fields_value('pr_desc');
								$projectURL = simple_fields_value('pr_url');
								$projectAttr = simple_fields_value('pr_techinfo');
								


								$memberType = simple_fields_value('membertype');
								$testimonialType = simple_fields_value('testimonialtype');
								$excerpt = simple_fields_value('memberquote');
								$memberTypeText = '';
								$testTypeText = '';
								$imgState = '';
								$testimonialContent = get_the_content();
								switch ($memberType) {
									case 'radiobutton_num_2':
									$memberTypeText = 'Personal';
									break;
									case 'radiobutton_num_3':
									$memberTypeText = 'Business';
									break;
									case 'radiobutton_num_4':
									$memberTypeText = 'Student';
									break;
								}
								switch ($testimonialType) {
									case 'radiobutton_num_2':
									$testTypeText = 'video';
									break;
									case 'radiobutton_num_3':
									$testTypeText = 'written';
									break;
									case 'radiobutton_num_4':
									$testTypeText = 'facebook';
									break;
									case 'radiobutton_num_5':
									$testTypeText = 'twitter';
									break;
								}
	                            if($testimonialContent=='') {
	                                $contentToggle = ' notext';
	                            } else {
	                                if ($testTypeText == 'facebook'){
	                                  $contentToggle = ' facewritten written';
	                                } else {
	                                    $contentToggle = '';
	                                }
	                            }
								if ($testTypeText=='written' && !has_post_thumbnail()) {
									$imgState = 'noimg';
								}
							?>
                        <div id="post-<?php the_ID(); ?>" class="entry-content <?php echo $memberTypeText ?>  <?php echo $testTypeText ?>  <?php echo $contentToggle ?>  <?php echo $imgState ?>">
							<?php

						// $cta_x = simple_fields_value('calloutx');
						// $cta_y = simple_fields_value('callouty');
						// $cta_color = simple_fields_value('callcolor');
						//if($cta_color == 'radiobutton_num_3') $cta_color = 'green';
								if($testTypeText!='video') {
									$newStr = get_the_title();
									$newStr = str_replace('<br />', '&nbsp;', $newStr);
									echo '<h1>Member Story: '.$newStr.'</h1>';
								}
			        			
			        			if ($testimonialEmbed !=='') {
			        				echo '<div class="testimonial-embed '.$testTypeText.$contentToggle.'">'.$testimonialEmbed;
			        				echo '</div>';
			        			} else if (has_post_thumbnail()) {
			        				echo '<div id="imgheader" class="imgHolder">'.the_post_thumbnail('full').'</div>';
			        			} 
	                        	if($testimonialContent!='') {
		                        	echo '<div class="testimonial-text"><div class="scrollcontent">'.$testimonialContent.'</div></div>';
	                        	}
	                        	$imgurl = wp_get_attachment_url( get_post_thumbnail_id() );
	                        	$twString = '<a class="twitter" target="_blank" href="https://twitter.com/intent/tweet?text=Member Story: '.get_the_title().'&url=https://www.mygenfcu.org/member-stories/&via=generationsfcu">Tweet</a>';
	                        	// $fbString = '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=//www.mygenfcu.org/member-stories/">Share on Facebook</a>';
	                        	$fbString = '<a class="facebook" target="_blank" href="https://www.facebook.com/dialog/feed?app_id=262042270668849&display=popup&caption=Member Story: '.get_the_title().'&link=https://www.mygenfcu.org/member-stories/&redirect_uri=https://www.mygenfcu.org/member-stories/&picture='.$imgurl.'">Share on Facebook</a>';
	                        	$liString = '<a class="linkedin" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.mygenfcu.org/member-stories/&title=Member Story: '.get_the_title().'&summary='.$excerpt.'&source=https://www.mygenfcu.org/member-stories/">Share on LinkedIn</a>';
	                        	$pnString = '<a class="pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=https://www.mygenfcu.org/member-stories/&description='.$excerpt.'&media='.$imgurl.'">Pin on Pinterest</a>';

	                        	echo '<div class="social"><span>Share:</span>'.$twString.$fbString.$liString.$pnString.'</div>';
	                        	// echo '<div class="social"><span>Share:</span>
	                        	// 	<a class="facebook" title="Share on Facebook" href="https://www.facebook.com/mygenfcu" target="_blank">Facebook</a>
	                        	// 	<a class="twitter" title="Share onTwitter" href="https://twitter.com/generationsfcu" target="_blank">Twitter</a>
	                        	// 	<a class="linkedin" title="share on LinkedIn" href="http://www.linkedin.com/company/495225?trk=tyah" target="_blank">LinkedIn</a>
	                        	// 	<a class="pinterest" title="Pin this to Pinterest" href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" target="_blank">Pinterest</a>
	                        	// 	</div>';
	                        	// echo '<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>';
	                        	// echo '</div>';
                        	?>
                        </div><!-- .entry-content -->
  	               <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
  	               <?php 
  	               		
  	               ?>
				<?php endwhile; // end of the loop. ?>
	</div>
	</div><!-- End Content row -->
</body>
</html>