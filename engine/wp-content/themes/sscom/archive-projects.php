<?php
/**
 * Template Name: Member Stories Archive Page
 * Description: The main template for the homepage
 *
 * @package required+ Foundation
 * @since required+ Foundation 0.2.0
 */

get_header(); ?>
    <!-- Row for main content area -->
    <div id="content" class="row fix">
        <div id="main" class="twelve columns info-page member-stories" role="main">
            <?php
                $blogID = 531;  
                $file_info = simple_fields_value("mainvis",$blogID);
                $page_headline = simple_fields_value("headline",$blogID);
                $intro_copy = simple_fields_value("introcopy",$blogID);

/*                if ($file_info) {
                    echo '<div id="mainvis" style="background:transparent url('.$file_info["url"].') fixed no-repeat;background-size:cover;"></div>';
                }
*/
                // echo '<div id="mainvis" class="fullgradient"></div>';

                if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                    // if IE<=8
                    echo '<div id="mainvis" style="background:transparent url(/assets/member_stories_bg.jpg) fixed no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></div>';
                } else {
                    echo '<div id="mainvis" style="background:transparent url(/assets/member_stories_bg.jpg) fixed no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;"></div>';
                }

                $args = array(
                    'post_type'=>'member-stories',
                    'hide_empty'     => 1,
                    'posts_per_page' => 20
                );
                //$query = new WP_Query($args);
            ?>
            <div class="post-box">
            <div class="entry-content">
                <h2>Real Members, Real Stories</h2>
                <p>Every service we provide is based on our members’ needs and feedback. Whether it’s a checking account or helping you to plan for retirement, we put your needs first. As a member, you’re more than a customer. You’re a co-owner.</p>
            </div><!-- .entry-content -->

            <div class="info">
                <div id="main-content" class="nonav"><div class="section">
                    <div class="share-cta">We’d love to hear from you: <a class="cta" href="/share-your-story/" onclick="_gaq.push(['_trackEvent', 'Button', 'Clicked', 'ShareStory'])" target="_self">Share Your Story</a></div>
                        <?php query_posts($args); while (have_posts() ) : the_post(); ?>
                        <?php 
                            $currID = $post->ID;

                            $memberSince = simple_fields_value('membersince');
                            $testimonialEmbed = simple_fields_value('membersource');
                            $lp_contentarea = simple_fields_value('contentarea');
                            $memberType = simple_fields_value('membertype');
                            $testimonialType = simple_fields_value('testimonialtype');
                            $memberTypeText = '';
                            $testTypeText = '';
                            $testTypePrompt = '';
                            $small_checked = simple_fields_value('usesmall');
                            $excerpt = simple_fields_value('memberquote');
                            switch ($memberType) {
                                case 'radiobutton_num_2':
                                $memberTypeText = 'Personal';
                                break;
                                case 'radiobutton_num_3':
                                $memberTypeText = 'Business';
                                break;
                                case 'radiobutton_num_4':
                                $memberTypeText = 'Student';
                                break;
                            }
                            switch ($testimonialType) {
                                case 'radiobutton_num_2':
                                $testTypeText = 'video';
                                $testTypePrompt = 'View their video testimonial';
                                break;
                                case 'radiobutton_num_3':
                                $testTypeText = 'written';
                                $testTypePrompt = 'Read their written testimonial';
                                break;
                                case 'radiobutton_num_4':
                                $testTypeText = 'facebook';
                                $testTypePrompt = 'Read their Facebook testimonial';
                                break;
                                case 'radiobutton_num_5':
                                $testTypeText = 'twitter';
                                $testTypePrompt = 'Read their Twitter testimonial';
                                break;
                            }
                            $testimonialContent = get_the_content();
                            if($testimonialContent=='') {
                                $contentToggle = ' notext';
                            } else {
                                if ($testTypeText == 'facebook'){
                                  $contentToggle = ' facewritten';
                                } else {
                                    $contentToggle = '';
                                }
                            }

                            $permalink = get_permalink($currID);
                            if (has_post_thumbnail() && !$small_checked && $testTypeText != 'facebook' && $testTypeText != 'twitter') {
                                $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($currID), 'full' );
                                $imgurl = $thumb['0'];
                                if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                                    // if IE<=8
                                    echo '<div data-pop="'.$permalink.'" class="thumb colorb '.$testTypeText.' '.$memberTypeText.$contentToggle.'" title="'.$testTypePrompt.'" style="background:transparent url('.$imgurl.') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);">';
                                } else {
                                    echo '<div data-pop="'.$permalink.'" class="thumb colorb '.$testTypeText.' '.$memberTypeText.$contentToggle.'" title="'.$testTypePrompt.'" style="background:transparent url('.$imgurl.') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">';
                                }
                            } else {
                                echo '<div data-pop="'.$permalink.'" class="thumb colorb on '.$testTypeText.' '.$memberTypeText.$contentToggle.'" title="'.$testTypePrompt.'">';
                            }
                            echo '<div class="caption">';
                            echo '<p class="quote">“'.$excerpt.'”</p>';
                            echo '</div>';
                            echo '<div class="info">';
                            if (has_post_thumbnail() && $small_checked || $testTypeText == 'facebook' || $testTypeText == 'twitter') {
                                $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($currID), 'full' );
                                $imgurl = $thumb['0'];
                                if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) {
                                    // if IE<=8
                                    echo '<span class="titlethumb" style="background:transparent url('.$imgurl.') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;behavior: url(/backgroundsize.htc);"></span>';
                                } else {
                                    echo '<span class="titlethumb" style="background:transparent url('.$imgurl.') center center no-repeat;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;"></span>';
                                }

                                echo '<div style="float:left;"><h1>'.get_the_title().'</h1>';
                                // echo '<h2>'.$memberTypeText.' member<br />since '.$memberSince.'</h2></div>';
                                echo '<h2>'.$memberTypeText.' Member</h2></div>';
                            } else {
                                echo '<h1>'.get_the_title().'</h1>';
                                // echo '<h2>'.$memberTypeText.' Member<br />since '.$memberSince.'</h2>';
                                echo '<h2>'.$memberTypeText.' Member</h2>';

                            }
                            echo '<a class="colorbox-link" href="'.$permalink.'" title="'.$testTypePrompt.'">'.$testTypePrompt.'</a>';
                            echo '</div>';
                            echo '</div>';
                        ?>
                   <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'requiredfoundation' ) . '</span>', 'after' => '</div>' ) ); ?>
                <?php endwhile; // end of the loop. ?>
            </div>
                    </div>                 
                    </div>
                </div>

        </div><!-- /#main -->
    </div><!-- End Content row -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
