			<!-- footer -->
			<footer class="footer" role="contentinfo">

			</footer>
			<!-- /footer -->
		<script>/*shout out to  mr. doob*/var canvas=document.createElement('canvas');canvas.width=self.innerWidth;canvas.height=self.innerHeight;canvas.style.position='absolute';canvas.style.left='0px';canvas.style.top='0px';document.body.appendChild(canvas);var context=canvas.getContext('2d');context.globalCompositeOperation='lighter';var size=3;var sizeHalf=size/2;for(var i=0;i<8;i++){size*=2;sizeHalf=size/2;for(var x=0;x<canvas.width;x+=size){for(var y=0;y<canvas.height;y+=size){var hue=Math.random()*(x+y)/2;var opacity=Math.random()*0.7;context.fillStyle='hsla('+hue+',50%,40%,'+opacity+')';if(Math.random()>0.5)drawSquare(x,y);if(Math.random()>0.5)drawTriangleLeft(x,y);if(Math.random()>0.5)drawTriangleRight(x,y);}}}function drawSquare(x,y){context.beginPath();context.moveTo(x,y);context.lineTo(x,y+size);context.lineTo(x+size,y+size);context.lineTo(x+size,y);context.fill();}function drawTriangleLeft(x,y){context.beginPath();context.moveTo(x,y);context.lineTo(x,y+size);context.lineTo(x+sizeHalf,y+sizeHalf);context.fill();}function drawTriangleTop(x,y){context.beginPath();context.moveTo(x,y);context.lineTo(x+size,y);context.lineTo(x+sizeHalf,y+sizeHalf);context.fill();}function drawTriangleRight(x,y){context.beginPath();context.moveTo(x+size,y);context.lineTo(x+size,y+size);context.lineTo(x+sizeHalf,y+sizeHalf);context.fill();}function drawTriangleBottom(x,y){context.beginPath();context.moveTo(x,y+size);context.lineTo(x+sizeHalf,y+sizeHalf);context.lineTo(x+size,y+size);context.fill();}</script>

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
	</body>
</html>
